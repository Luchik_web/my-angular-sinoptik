'use strict';

var express, bodyParser, methodOverride, app, port;

express = require('express');
bodyParser = require('body-parser');
methodOverride = require('method-override');
app = express();
port = 1337;

app.use(express.static(__dirname + '/app')); 
app.use(bodyParser.json()); // parse application/json
app.use(bodyParser.json({ type: 'application/vnd.api+json' })); // parse application/vnd.api+json as json
app.use(bodyParser.urlencoded({'extended':'true'}));
app.use(methodOverride('X-HTTP-Method-Override')); // override with the X-HTTP-Method-Override header in the request

app.use('/config', express.static(__dirname + '/app/config'));
app.use('/fixtures', express.static(__dirname + '/app/fixtures'));
app.use('/vendor', express.static(__dirname + '/app/vendor'));
app.use('/ui', express.static(__dirname + '/app/ui'));
app.use('/core', express.static(__dirname + '/app/core'));
app.all('/', function(req, res, next) {
    res.sendfile('index.html', {root: __dirname + '/app'});
});

app.listen(port, function() {
    console.log('Start server on %s:%d', 'http://127.0.0.1', port);
});