/**
 * Definition for Routes. Contains application external routes
 *
 * @returns {object}
 */
define(function() {
    'use strict';

    var _routes;
    _routes = {
        // ----- API URLs -----
        ///////////////////////////////////////
        // Auth
        ///////////////////////////////////////
        'User__login': ['POST', '{apiUrl}/user/login'],
        'User__logout': ['POST', '{apiUrl}/user/login'],
        ///////////////////////////////////////
        // User
        ///////////////////////////////////////
        'Weather__list': ['GET', '{apiUrl}/city/list'],
        'Weather__getById': ['GET', '{apiUrl}/city/get', ['id']],
//        ['zip=94040','lat=35&lon=139','q=London,us']
        /////////////////////////////////////////////
        // External Urls
        ///////////////////////////////////////
    };

    return _routes;
});