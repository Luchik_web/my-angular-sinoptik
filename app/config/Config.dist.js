/**
 * Definition for singleton Config. Contains main configurations of the application
 * Returns an instance of Config that will be used as singleton in the application
 *
 * @returns {Config} Instance of Config
 */
define(function(require) {
    'use strict';

    var _routes;
    _routes = require('./routes');

    function _Config () {

        var _config, _siteUrl, _apiURL, _apiVersion;

        _siteUrl = location.origin;
        _apiURL = 'http://127.0.0.1:1337';
        _apiVersion = 'v1.0';

        _config = {
            'siteUrl': _siteUrl,
            'weatherApiKey' : 'ead6b24e32e2e60bf36ae25334a7577a',
            'weatherApiUrl' : 'http://api.openweathermap.org/data/2.5',
            'whiteList': [_siteUrl, 'http://api.openweathermap.org'],
            'request': _routes,
            'defaultLocale' : 'en_GB',
            'version': _apiVersion
        };

        for (var  i in _config.request) {
            _config.request[i][1] = _config.request[i][1].replace('{apiUrl}', _apiURL);
            _config.request[i][1] = _config.request[i][1].replace('{weatherApiKey}', _config.weatherApiKey);
            _config.request[i][1] = _config.request[i][1].replace('{v}', _apiVersion);
            _config.request[i] = _config.request[i];
        }

        return _config;
    }

    return _Config();
});