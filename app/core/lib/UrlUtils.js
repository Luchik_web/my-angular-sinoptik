/**
 * This module contains utilitary functions used by other modules
 *
 * JSON object returns containing functions.
 *
 */
define(function (require) {

    var UrlUtils = {
        /**
         * Trims Url, appends optional ? or & and appends query parameter
         *
         * @param {String} url
         * @param {String} paramName
         * @param {String} paramValue
         * @returns {String}
         */
        appendParamToUrl: function (url, paramName, paramValue) {

            if (typeof paramName === 'undefined') {
                return url;
            }
            if (typeof paramValue === 'undefined') {
                return url;
            }
            url = this.normalizeUrl(url);

            if (!(this.endsWith(url, '?') || this.endsWith(url, '&'))) {
                url += '&';
            }

            url += paramName + '=' + paramValue;

            return url;
        },
        endsWith: function (str, substr) {

            return str.lastIndexOf(substr) === (str.length - substr.length);
        },
        normalizeUrl: function (url) {
            url = url.trim();
            return url.indexOf('?') < 0 ? url + '?' : url;
        },
        serializeDataToUrl: function (url, data, paramPrefix, isStrictFormMode) {
            var that;
            that = this;

            isStrictFormMode = (true === isStrictFormMode) ? true : false;
            url = _serialize(url, data, paramPrefix, isStrictFormMode);

            url = url.replace(/{\b[^<]*(?:(?!<\/script>)<[^<]*)*}/gi, '');

            return url;

            function _serialize(url, data, paramPrefix, isStrictFormMode) {
                var key, prefix;

                prefix = paramPrefix || '';

                for (key in data) {

                    var paramName = prefix + key;
//
//                    if (!isStrictFormMode && typeof data[key] === 'object' && null !== data[key]) {
//                        url = _serialize(url, data[key], paramName + '-', isStrictFormMode);
//                        continue;
//                    }

                    if (url.indexOf(['{' + key + '}']) > -1) {
                        url = url.trim().replace('{' + key + '}', (data[key] === null) ? '' : data[key]);
                        data[key] = undefined;

                    } else if (!isStrictFormMode) {
                        url = that.appendParamToUrl(url, paramName, data[key]);
                    }
                }

                return url;

            }
        }

    };

    return UrlUtils;
});
