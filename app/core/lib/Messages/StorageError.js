/**
 * Define Storage Error
 *
 * @returns {Error} Error object
 */
define(function(require) {
    'use strict';
    
    return Error;
});