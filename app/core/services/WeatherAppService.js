/**
 * Definition for WeatherAppService singlton
 * Returns an instance of WeatherAppService
 *
 * @author Luchik
 *
 * @requires IRequestService
 *
 * @param {require} require
 * @returns {WeatherAppService} Definition for WeatherAppService
 */
define(function (require) {
    'use strict';
    var IPromiseAdapter = require('IPromiseAdapter'),
        IWeatherRepository = require('IWeatherRepository'),
        IWeather = require('IWeather');

    /**
     * -----------------------------------------------------------------------------------------------------------------
     * Constructor for Weather Preference Application Service
     * -----------------------------------------------------------------------------------------------------------------
     *
     * @see IRequestService
     *
     * @public
     * @constructor
     * @returns {undefined}
     */

    function WeatherAppService() {
        this.selectedWether = null;
    }

    /**
     * Weather list
     *
     * Asynchronous method
     *
     * @returns {Promise}
     */

    WeatherAppService.prototype.list1DayWetherByCoordinatesAsync = function () {
        var wetherList;
        wetherList = IWeatherRepository.getWetherAll();

        if (wetherList.length > 0) {
            return IPromiseAdapter.resolve(wetherList);
        }

        if (!navigator || !navigator.geolocation) {
            return IPromiseAdapter.resolve([]);
        }

        var deferred;
        deferred = IPromiseAdapter.defer();
        navigator.geolocation.getCurrentPosition(function(position) {
            deferred.resolve(position);
        }, function() {
            deferred.reject();
        });

        return deferred.promise
            .then(function(position) {
                var latitude, longitude;
                latitude = position.coords.latitude;
                longitude = position.coords.longitude;
                return IWeather.get1DayWeatherAsync({
                    'latitude': latitude,
                    'longitude': longitude,
                });
            })
            .then(function (response) {
                IWeatherRepository.setWether(response);
                return [response];
            }, function() {
                return [];
            });
    };

    /**
     * Weather get 1 day
     *
     * Asynchronous method
     *
     * @returns {Promise}
     */
    WeatherAppService.prototype.get1DayWetherByCoordinatesAsync = function (latitude, longitude) {
        var weather;
        weather = IWeatherRepository.getWetherByCoordinates(latitude, longitude);

        if (weather && weather.day) {
            return IPromiseAdapter.resolve(weather);
        }

        return IWeather.get1DayWeatherAsync({
                'latitude': latitude,
                'longitude': longitude,
            })
            .then(function (response) {
                IWeatherRepository.setWether(response);
                return response.data;
            });
    };

    /**
     * Delete wether by id
     *
     * Synchronous method
     *
     * @returns {Promise}
     */
    WeatherAppService.prototype.deleteWetherById = function (id) {
        IWeatherRepository.removeWether(id);
    };

    /**
     * Weather get 5 days
     *
     * Asynchronous method
     *
     * @returns {Promise}
     */
    WeatherAppService.prototype.get5DaysWetherByIdAsync = function (id) {
        var weather;
        if (!id) {
            return IPromiseAdapter.resolve(null);
        }

        weather = IWeatherRepository.getWetherById(id);
        if (weather && weather.day && weather.list) {
            return IPromiseAdapter.resolve(weather);
        }
        return IWeather.get5DaysWeatherAsync({
                'latitude': weather.city.latitude,
                'longitude': weather.city.longitude,
            })
            .then(function (response) {
                IWeatherRepository.setWether(response);
                return response;
            });

    };

    /**
     * Weather get 5 days
     *
     * Asynchronous method
     *
     * @returns {Promise}
     */
    WeatherAppService.prototype.get7DaysWetherByIdAsync = function (id) {
        var weather;
        if (!id) {
            return IPromiseAdapter.resolve(null);
        }

        weather = IWeatherRepository.getWetherById(id);
        if (weather && weather.day && weather.list) {
            return IPromiseAdapter.resolve(weather);
        }
        return IWeather.get7DaysWeatherAsync({
                'latitude': weather.city.latitude,
                'longitude': weather.city.longitude,
            })
            .then(function (response) {
                IWeatherRepository.setWether(response);
                return response;
            });

    };


    // Returns an instance of WeatherAppService. This instance will be used as singlton in the applicatio
    return new WeatherAppService();
});
