/**
 * Definition for UserAppService singlton
 * Returns an instance of UserAppService
 *
 * @author Luchik
 *
 * @requires IRequestService
 *
 * @param {require} require
 * @returns {UserAppService} Definition for UserAppService
 */
define(function(require) {
    'use strict';

    /**
     * -----------------------------------------------------------------------------------------------------------------
     * Constructor for User Preference Application Service
     * -----------------------------------------------------------------------------------------------------------------
     *
     * @see IRequestService
     *
     * @public
     * @constructor
     * @returns {undefined}
     */

    function CityAppService(IRequestService) {
        this.IRequestService = IRequestService;
    }

    /**
     * Login
     *
     * Asynchronous method
     *
     * @param {string} login
     * @param {string} password
     * @returns {UserAggregate|null}
     */
    UserAppService.prototype.loginAsync = function(login, password) {

        return this.IRequestService.request('Auth__login', {
            'login': login,
            'password' : password
        });
    };

    /**
     * Check if User is logged in
     *
     * Synchronous method
     *
     * @returns {UserAggregate|null}
     */
    UserAppService.prototype.isLoggedIn = function() {
        return true;
    };

    /**
     * Logout
     *
     * @returns {undefined}
     */
    UserAppService.prototype.logout = function() {
        return this.IRequestService.request('Auth__logout', {});
    };

    // Returns an instance of UserAppService. This instance will be used as singlton in the applicatio
    return UserAppService;
});
