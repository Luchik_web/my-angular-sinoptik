/**
 * Definition for Local Storage
 * Returns localStorage object
 *
 * @see localStorage
 *
 * @returns {localStorage} localStorage
 */
define(function(require) {
    "use strict";

    var StorageError = require('library/Messages/StorageError');

    // Check if local storage exists
    if (typeof (localStorage) === "undefined") {
        throw new StorageError('Sorry! No web storage support..', 'LocalStorage');
    }

    var LocalStorage = {
        /**
         * Get key for item with i index
         *
         * @param {string|number} key
         * @param {string} val
         * @returns {undefined}
         */
        getAll: function() {
            return localStorage;
        },
        /**
         * Get all records by key (start of the array)
         *
         * @private
         * @param {string} storageKey Key  name
         * @returns {array}
         */
        searchAll: function(storageKey) {
            var results, i, keys;

            results = [];
            keys = Object.keys(localStorage);
            for (i in keys) {
                if (0 === keys[i].indexOf(storageKey)) {
                    var item = localStorage.getItem(keys[i]);
                    item = JSON.parse(item);
                    results.push(item);
                }
            }

            return results;
        },
        /**
         * Get item from the storage by key
         *
         * @param {string|number} key
         * @returns {string|number|null} Returns null if object is not in storage
         */
        getItem: function(key) {
            var item = localStorage.getItem(key);
            item = JSON.parse(item);
            return item;
        },
        /**
         * Set item to storage with defined key
         *
         * @param {string|number} key
         * @param {string} val
         * @returns {undefined}
         */
        setItem: function(key, val) {
            val = JSON.stringify(val);
            localStorage.setItem(key, val);
        },
        /**
         * Remove item from the storage by key
         *
         * @param {string|number} key
         * @returns {undefined}
         */
        removeItem: function(key) {
            localStorage.removeItem(key);
        },
        /**
         * Clear all storage
         *
         * @returns {undefined}
         */
        clear: function() {
            localStorage.clear();
        }
    };

    // Return storage interface
    return LocalStorage;
});