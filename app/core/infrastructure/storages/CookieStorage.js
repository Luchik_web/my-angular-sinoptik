/**
 * Definition for Cookie Storage
 * Returns cookiesStorage object
 *
 *
 * @returns {cookiesStorage} CookiesStorage
 */
define(function(require) {
    "use strict";

    var StorageError = require('library/Messages/StorageError');

    // Check for cookies support
    if (typeof (document.cookie) === "undefined") {
        throw new StorageError('Sorry! No cookies support...', 'CacheStorage');
    }

    var CookiesStorage = {
        /**
         * Get all data from the storage
         *
         * @returns {object}
         */
        getAll: function() {
            var i, iLength, cookiearray, cookies, key, value;

            // Get all the cookies pairs in an array
            cookiearray = document.cookie.split(';');
            iLength = cookiearray.length;
            cookies = {};
            // Take (key;value) pair out of the cookies array
            for (i = 0; i < iLength; i++) {
                key = cookiearray[i].split('=')[0];
                value = cookiearray[i].split('=')[1];
                cookies[key] = value;
            }
            return cookies;
        },
        /**
         * Get all records by key (start of the array)
         *
         * @private
         * @param {string} storageKey Key  name
         * @returns {array}
         */
        searchAll: function(storageKey) {
            var i, iLength, cookiearray, cookies, key, value, results;

            results = [];
            // Get all the cookies pairs in an array
            cookiearray = document.cookie.split(';');
            iLength = cookiearray.length;
            cookies = {};
            // Take (key;value) pair out of the cookies array
            for (i = 0; i < iLength; i++) {
                key = cookiearray[i].split('=')[0];
                if (0 === key.indexOf(storageKey)) {
                    value = cookiearray[i].split('=')[1];
                    results.push(value);
                }
            }
            return results;
        },
        /**
         * Get item from the dataStored by key
         *
         * @param {string|number} key
         * @returns {string|number|null} Returns null if object is not in dataStored
         */
        getItem: function(key) {
            var i, iLength, keyEQ, cookies, cookie;

            keyEQ = key + "=";

            // Get all the cookies pairs in an array
            cookies = document.cookie.split(';');
            iLength = cookies.length;

            for (i = 0; i < iLength; i++) {
                cookie = cookies[i];
                while (' ' === cookie.charAt(0)) {
                    cookie = cookie.substring(1, cookie.length);
                }

                if (0 === cookie.indexOf(keyEQ)) {
                    return decodeURIComponent(cookie.substring(keyEQ.length, cookie.length));
                }
            }
            return null;
        },
        /**
         * Set item to dataStored with defined key
         *
         * @param {string|number} key
         * @param {string} val
         * @param {number} days
         * @returns {undefined}
         */
        setItem: function(key, val, days) {
            var expires;
            expires = "";

            if (!days) {
                days = 7;
            }
            expires = new Date();
            expires.setTime(expires.getTime() + (days * 24 * 60 * 60 * 1000));
            expires = "; expires=" + expires.toGMTString();
            
            document.cookie = key + "=" + encodeURIComponent(val) + expires + "; path=/";
        },
        
        /**
         * Remove item from the dataStored by key
         *
         * @param {string|number} key
         * @returns {undefined}
         */
        removeItem: function(key) {
            var expires;

            expires = new Date(1900);
            expires.setTime(expires.getTime());
            expires = expires.toGMTString();

            document.cookie = key + "=; expires=" + expires + "; path=/";
        },
        /**
         * Clear all dataStored
         *
         * @returns {undefined}
         */
        clear: function() {
            var i, iLength, cookies, cookie, posEQ, key, expires;

            // Get all the cookies pairs in an array
            cookies = document.cookie.split(';');
            iLength = cookies.length;

            expires = new Date(1900);
            expires.setTime(expires.getTime());
            expires = expires.toGMTString();

            for (i = 0; i < iLength; i++) {
                cookie = cookies[i];
                posEQ = cookie.indexOf("=");
                key = (posEQ > -1) ? cookie.substr(0, posEQ) : cookie;
                document.cookie = key + "=; expires=" + expires + "; path=/";
            }
        }
    };

    return CookiesStorage;
});