/**
 * Definition for Session Storage
 * Returns sessionStorage object
 *
 * @see sessionStorage
 *
 * @returns {sessionStorage} sessionStorage
 */
define(function(require) {
    "use strict";

    var StorageError = require('library/Messages/StorageError');

    // Check if session storage exists
    if (typeof (sessionStorage) === "undefined") {
        throw new StorageError('Sorry! No web storage support..', 'SessionStorage');
    }

    var SessionStorage = {
        /**
         * Get key for item with i index
         *
         * @param {string|number} key
         * @param {string} val
         * @returns {undefined}
         */
        getAll: function(i) {
            return sessionStorage;
        },
        /**
         * Get all records by key (start of the array)
         *
         * @private
         * @param {string} storageKey Key  name
         * @returns {array}
         */
        searchAll: function(storageKey) {
            var results, i, keys;

            results = [];
            keys = Object.keys(sessionStorage);
            for (i in keys) {
                if (0 === keys[i].indexOf(storageKey)) {
                    results.push(sessionStorage.getItem(keys[i]));
                }
            }

            return results;
        },
        /**
         * Get item from the storage by key
         *
         * @param {string|number} key
         * @returns {string|number|null} Returns null if object is not in storage
         */
        getItem: function(key) {
            return sessionStorage.getItem(key);
        },
        /**
         * Set item to storage with defined key
         *
         * @param {string|number} key
         * @param {string} val
         * @returns {undefined}
         */
        setItem: function(key, val) {
            sessionStorage.setItem(key, val);
        },
        /**
         * Remove item from the storage by key
         *
         * @param {string|number} key
         * @returns {undefined}
         */
        removeItem: function(key) {
            sessionStorage.removeItem(key);
        },
        /**
         * Clear all storage
         *
         * @returns {undefined}
         */
        clear: function() {
            sessionStorage.clear();
        }
    };

    // Return storage interface
    return SessionStorage;
});