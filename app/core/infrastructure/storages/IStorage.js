/**
 * Interface for dataStored
 *
 * @returns {IStorage} IStorage
 */
define(function() {

    var dataStored = {};
    var IStorage = {
        /**
         * Get all data from the storage
         *
         * @returns {object}
         */
        getAll: function() {
            return Utils.Object.clone(dataStored);
        },
        /**
         * Get all records by key (start of the array)
         *
         * @private
         * @param {string} storageKey Key  name
         * @returns {array}
         */
        searchAll: function(storageKey) {
            var results, key, dataStoredClone;

            results = [];
            dataStoredClone = Utils.Object.clone(dataStored);
            for (key in dataStoredClone) {
                if (0 === key.indexOf(storageKey)) {
                    results.push(dataStored[key]);
                }
            }

            return results;
        },
        /**
         * Get item from the dataStored by key
         *
         * @param {string|number} key
         * @returns {string|number|null} Returns null if object is not in dataStored
         */
        getItem: function(key) {
            if ('undefined' === typeof dataStored[key]) {
                return null;
            }
            return dataStored[key];
        },
        /**
         * Set item to dataStored with defined key
         *
         * @param {string|number} key
         * @param {string} val
         * @returns {undefined}
         */
        setItem: function(key, val) {
            dataStored[key] = val;
        },
        /**
         * Remove item from the dataStored by key
         *
         * @param {string|number} key
         * @returns {undefined}
         */
        removeItem: function(key) {
            delete dataStored[key];
        },
        /**
         * Clear all dataStored
         *
         * @returns {undefined}
         */
        clear: function() {
            dataStored = {};
        }
    };

    // Return dataStored interface
    return IStorage;
});