define(function (require) {
    'use strict';

    var Config = require('config/Config'),
        IRequestService = require('IRequestService');

    /**
     * -----------------------------------------------------------------------------------------------------------------
     * Constructor for OpenWeatherMap
     * -----------------------------------------------------------------------------------------------------------------
     *
     * @public
     * @constructor
     * @returns {undefined}
     */

    function OpenWeatherMap()
    {
        this.appid = Config.weatherApiKey;
        this.wether1DayApiUrl = Config.weatherApiUrl + '/weather';
        this.wether5DaysApiUrl = Config.weatherApiUrl + '/forecast';
        this.wether7DaysApiUrl = Config.weatherApiUrl + '/forecast/daily?cnt=7';
    }

    /**
     * Get 1 day wether
     *
     * API respond
     * {
     *   "coord":{"lon":139,"lat":35},
     *   "sys":{"country":"JP","sunrise":1369769524,"sunset":1369821049},
     *   "weather":[{"id":804,"main":"clouds","description":"overcast clouds","icon":"04n"}],
     *   "main":{"temp":289.5,"humidity":89,"pressure":1013,"temp_min":287.04,"temp_max":292.04},
     *   "wind":{"speed":7.31,"deg":187.002},
     *   "rain":{"3h":0},
     *   "clouds":{"all":92},
     *   "dt":1369824698,
     *   "id":1851632,
     *   "name":"Shuzenji",
     *   "cod":200
     * }
     * @retur {Promise<object>}
     */
    OpenWeatherMap.prototype.get1DayWeatherAsync = function (params)
    {
        return this._getWeatherAsync(this.wether1DayApiUrl, params)
            .then(function(response) {
                var day, resultObject = {};
                // Main
                resultObject['_ID'] =  response.id;
                resultObject['city'] =  {};
                resultObject['city']['name'] =  response.name;
                resultObject['city']['country'] =  response.sys.country;
                resultObject['city']['latitude'] =  response.coord.lat;
                resultObject['city']['longitude'] =  response.coord.lon;
                
                // Extra data
                resultObject['coord'] =  response.coord;

                // temperature
                day = {};
                day['date'] = new Date(response.dt*1000);
                day['main'] = response.main;
                day['weather'] = response.weather;
                day['wind'] = response.wind;
                day['rain'] = response.rain;
                day['clouds'] = response.clouds;
                resultObject['day'] =  day;
                return resultObject;
            });
    };

    /**
     * Get 5 days wether
     *
     * API respond
     * {
     *  "city":{
     *     "id":1851632,
     *     "name":"Shuzenji",
     *     "coord":{"lon":138.933334,"lat":34.966671},
     *     "country":"JP",
     *  }
     *  "cod":"200",
     *  "message":0.0045,
     *  "cnt":38,
     *  "list":[{
     *      "dt":1406106000,
     *      "main":{
     *         "temp":298.77,
     *         "temp_min":298.77,
     *         "temp_max":298.774,
     *         "pressure":1005.93,
     *         "sea_level":1018.18,
     *         "grnd_level":1005.93,
     *         "humidity":87
     *         "temp_kf":0.26
     *      },
     *      "weather":[{"id":804,"main":"Clouds","description":"overcast clouds","icon":"04d"}],
     *      "clouds":{"all":88},
     *      "wind":{"speed":5.71,"deg":229.501},
     *      "sys":{"pod":"d"},
     *      "dt_txt":"2014-07-23 09:00:00"
     *   }]
     * }
     *
     * @retur {Promise<object>}
     */
    OpenWeatherMap.prototype.get5DaysWeatherAsync = function (params)
    {
        return this._getWeatherAsync(this.wether5DaysApiUrl, params)
            .then(function(response) {
                var i, day, list, resultObject = {};
                // Main
                resultObject['_ID'] =  response.city.id;
                resultObject['city'] =  {};
                resultObject['city']['name'] =  response.city.name;
                resultObject['city']['country'] =  response.city.country;
                resultObject['city']['latitude'] =  response.city.coord.lat;
                resultObject['city']['longitude'] =  response.city.coord.lon;
                
                // Extra data
                resultObject['coord'] =  response.city.coord;

                // temperature
                resultObject['list'] =  [];
                for (i in response.list) {
                    day = {};
                    day['date'] = new Date(response.list[i].dt*1000);
                    day['main'] = response.list[i].main;
                    day['weather'] = response.list[i].weather;
                    day['wind'] = response.list[i].wind;
                    day['rain'] = response.list[i].rain;
                    day['clouds'] = response.list[i].clouds;
                    resultObject['list'].push(day);
                }

                return resultObject;
            });
    };

    /**
     * Get 5 days wether
     *
     * API respond
     * {
     *  "city":{
     *     "id":1851632,
     *     "name":"Shuzenji",
     *     "coord":{"lon":138.933334,"lat":34.966671},
     *     "country":"JP",
     *  }
     *  "cod":"200",
     *  "message":0.0045,
     *  "cnt":38,
     *  "list":[{
     *      "dt":1406106000,
     *      "main":{
     *         "temp":298.77,
     *         "temp_min":298.77,
     *         "temp_max":298.774,
     *         "pressure":1005.93,
     *         "sea_level":1018.18,
     *         "grnd_level":1005.93,
     *         "humidity":87
     *         "temp_kf":0.26
     *      },
     *      "weather":[{"id":804,"main":"Clouds","description":"overcast clouds","icon":"04d"}],
     *      "clouds":{"all":88},
     *      "wind":{"speed":5.71,"deg":229.501},
     *      "sys":{"pod":"d"},
     *      "dt_txt":"2014-07-23 09:00:00"
     *   }]
     * }
     *
     * @retur {Promise<object>}
     */
    OpenWeatherMap.prototype.get7DaysWeatherAsync = function (params)
    {
        return this._getWeatherAsync(this.wether7DaysApiUrl, params)
            .then(function(response) {
                var i, day, resultObject = {};
                // Main
                resultObject['_ID'] =  response.city.id;
                resultObject['city'] =  {};
                resultObject['city']['name'] =  response.city.name;
                resultObject['city']['country'] =  response.city.country;
                resultObject['city']['latitude'] =  response.city.coord.lat;
                resultObject['city']['longitude'] =  response.city.coord.lon;
      
                // Extra data
                resultObject['coord'] =  response.city.coord;

                // temperature
                resultObject['list'] =  [];
                for (i in response.list) {
                    day = {};
                    day['date'] = new Date(response.list[i].dt*1000);
                    day['main'] = {
                        'temp': response.list[i].temp.day, //morning eve
                        'humidity': response.list[i].humidity,
                        'temp_max': response.list[i].temp.max,
                        'temp_min': response.list[i].temp.min,
                    };
                    day['weather'] = response.list[i].weather;
//                    day['wind'] = response.list[i].wind;
//                    day['rain'] = response.list[i].rain;
                    day['clouds'] = response.list[i].clouds;
                    resultObject['list'].push(day);
                }

                return resultObject;
            });
    };

    /**
     * Get wether by url and parameters
     *
     * @retur {Promise<object>}
     */
    OpenWeatherMap.prototype._getWeatherAsync = function (url, params)
    {
        var requestParams;
        requestParams = {
            appid: this.appid
        };

        // lat={lat}&lon={lon}
        // for example lat=35&lon=139
        requestParams['lat'] = params.latitude;
        requestParams['lon'] = params.longitude;

        // zip={zip},{country}
        // for example zip=94040,us

        return IRequestService.direct('GET', url, requestParams)
            .then(function(response) {
                return response.data;
            });
    };

    // Returns an instance of OpenWeatherMap. This instance will be used as singlton in the applicatio
    return new OpenWeatherMap();
});
