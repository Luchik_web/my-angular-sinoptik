define(function(require) {
    'use strict';


    require('angular');

    var Config = require('config/Config'),
        IServiceUrlResolver = require('IServiceUrlResolver');

    var ngInjector, AngularHttpAdapter;
    ngInjector = angular.injector(["ng"]);

    /**
     * Angular adapter. Angular $http required to be set!!!!
     */
    var AngularHttpAdapter = ngInjector.invoke(['$http', function ($http) {
        return {
            $httpProvider: $http,

            init: function($httpProvider) {
                this.$httpProvider = $httpProvider;
            },

            direct: function(method, url, params, headers) {
                var request = {};
                request.url = url;
                request.method = method;
                request.params = params;
                if (headers) {
                    request.headers = headers;
                }
                return this.$httpProvider(request);
            },

            request: function(urlAlias, request) {
                _preProcessRequest(urlAlias, request);
                return this.$httpProvider(request)
            },

            clear: function(urlAlias, request) {
                _preProcessRequest(urlAlias, request);
                return this.$httpProvider(request)
            }
        };
    }]);


    return AngularHttpAdapter;

    /**
     *
     * @param {string} urlAlias
     * @param {object} request
     * @returns {undefined}
     */
    function _preProcessRequest(urlAlias, request)
    {
        var httpMethod, urlTemplate;
        httpMethod = Config.request[urlAlias][0];
        urlTemplate = Config.request[urlAlias][1];
        request.method = httpMethod;
        request.url = IServiceUrlResolver.serviceUrl(urlAlias, request.data);
//        request.withCredentials = true;
    }
});