/**
 * Definition for GenericRequestServiceAsync
 * Returns constructor for GenericRequestServiceAsync. Should be used with [new] statement
 *
 * @param {require} require
 * @returns {GenericRequestServiceAsync} Definition for GenericRequestServiceAsync
 */
define(function(require) {
    "use strict";

    var IHttpAdapter = require('IHttpAdapter');

    /**
     * Local Repository class
     *
     * @see lib/CollectionClass
     * @requires CollectionClass
     * @requires localStorage
     *
     * @returns {undefined}
     */
    function GenericRequestServiceAsync() {
        this._private_ = {
            requestFilters: [],
            responseFilters: [],
            errorFilters: []
        };
    }

    /**
     * GET request to Web-frontend-app backend
     *
     * @param {String} urlAlias - alias of base URL defined in Config module
     * @param {object} params - data object to be passed to server
     * @returns {IPromise} - returns response data eventually (asynchroneously)
     */
    GenericRequestServiceAsync.prototype.request = function (urlAlias, params) {

        var responseFilters = this._private_.responseFilters,
            requestFilters = this._private_.requestFilters,
            errorFilters = this._private_.errorFilters;

        var request;
        request = _createPackageFromParams(params);
        request = _applyFilters(requestFilters, request);

        return IHttpAdapter.request(urlAlias, request)
            .then(function(response) {
                _applyFilters(responseFilters, response);
                return response.data;

            }, function(response) {
                _applyFilters(errorFilters, response);
                return response;
            });
    };

    /**
     * GET request to Web-frontend-app backend
     *
     * @param {String} urlAlias - alias of base URL defined in Config module
     * @param {object} params - data object to be passed to server
     * @returns {IPromise} - returns response data eventually (asynchroneously)
     */
    GenericRequestServiceAsync.prototype.direct = function (method, url, params) {
        return IHttpAdapter.direct(method, url, params);
    };

    /**
     * Clear method
     *
     * @public
     * @param {string} urlAlias
     * @param {object} params
     * @returns {undefined}
     */
    GenericRequestServiceAsync.prototype.clear = function (urlAlias, params) {
        var responseFilters = this._private_.responseFilters,
            requestFilters = this._private_.requestFilters;
        
        var request;
        request = _createPackageFromParams(params);
        request = _applyFilters(requestFilters, request);

        return IHttpAdapter.clear(urlAlias, request)
            .then(function(response) {
                _applyFilters(responseFilters, response);
                return response;
            });
    };

    /**
     * Request Service filters functions
     * 
     * @param {function} callback
     */

    GenericRequestServiceAsync.prototype.registerRequestFilter = function(callback) {
        if ('function' !== typeof callback) {
            throw new Error('Function is expected, ' + (typeof callback) + ' is provided');
        }
        this._private_.requestFilters.push(callback);
    };

    GenericRequestServiceAsync.prototype.registerResponseFilter = function(callback) {
        if ('function' !== typeof callback) {
            throw new Error('Function is expected, ' + (typeof callback) + ' is provided');
        }
        this._private_.responseFilters.push(callback);
    };

    GenericRequestServiceAsync.prototype.registerErrorFilter = function(callback) {
        if ('function' !== typeof callback) {
            throw new Error('Function is expected, ' + (typeof callback) + ' is provided');
        }
        this._private_.errorFilters.push(callback);
    };

    GenericRequestServiceAsync.prototype.unregisterAllFilters = function() {
        this._private_.requestFilters = [];
        this._private_.responseFilters = [];
        this._private_.errorFilters = [];
    };


////////////////////////////////////////////////////////////////////////////////////////

    function _applyFilters(filtersArr, envelope) {
        var i;

        for (i = 0; i < filtersArr.length; i++) {
            envelope = filtersArr[i](envelope);
        }

        return envelope;
    }

    function _createPackageFromParams(params) {
        return {
            headers: {},
            params: params
        };
    }

    return new GenericRequestServiceAsync();
});
