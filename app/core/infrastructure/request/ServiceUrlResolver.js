/**
 * Definition for IServiceUrlResolver
 * Returns singleton. Should be used without [new] statement
 *
 * @param {require} require
 * @returns {IServiceUrlResolver}
 */
define(function(require) {
    "use strict";

    var Config = require('config/Config'),
        UrlUtils = require('library/UrlUtils');

    var Config = require('config/Config'),
        UrlUtils = require('library/UrlUtils');

    function ServiceUrlResolver() {

    }


    /**
     * Returns Url for calling service. Url optionally includes envelope data as its query parameters.
     *
     * @param {String} urlAlias - alias of base URL defined in Config module
     * @param {object} envelope - data object which needs to be passed via Url parameters
     * @param {string} httpMethod
     * @returns {String}
     */
    ServiceUrlResolver.prototype.serviceUrl = function (urlAlias, envelope) {
        var urlTemplate, httpMethod,
            url, isStrictFormMode;

        httpMethod = Config.request[urlAlias][0];
        urlTemplate = Config.request[urlAlias][1];
        if ('string' !== typeof urlTemplate || '' === urlTemplate) {
            throw new Error('url for request is undefined! ' + urlAlias, 'ServiceUrlResolver');
        }

        isStrictFormMode = ('POST' === httpMethod || 'PUT' === httpMethod) ? true : false;
        url = UrlUtils.serializeDataToUrl(urlTemplate, envelope || {}, '', isStrictFormMode);

        return url;
    };

    return new ServiceUrlResolver();
});