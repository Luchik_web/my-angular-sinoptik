/**
 * Definition for Locale Repository singlton
 * Returns an instance of Locale Repository
 *
 * @author Luchik
 *
 * @requires ICookieStorage
 *
 * @param {require.js} require
 * @returns {LocaleRepository} Definition for LocaleRepository
 */
define(function (require) {
    'use strict';

    var IStorage = require('ILocalStorage');
    var _KEY_ = 'x-web-locale';

    /**
     * LocaleRepository constructor
     *
     * @constructor
     * @returns {constructor} Constructor for {LocaleRepository}
     */
    function LocaleRepository() {
    }

    /**
     * Get current Locale
     *
     * @returns {string|null}
     */
    LocaleRepository.prototype.getLocale = function () {
        return IStorage.getItem(_KEY_);
    };

    /**
     * Save current Locale
     *
     * @param {string} locale
     * @return {undefined}
     */
    LocaleRepository.prototype.setLocale = function (locale) {
        if (locale) {
            IStorage.setItem(_KEY_, locale);
        }
    };

    /**
     * Remove current Locale
     *
     * @return {undefined}
     */
    LocaleRepository.prototype.removeLocale = function () {
        IStorage.removeItem(_KEY_);
    };

    // Returns an instance of LocaleRepository. This instance will be used as singlton in the application
    return new LocaleRepository();
});