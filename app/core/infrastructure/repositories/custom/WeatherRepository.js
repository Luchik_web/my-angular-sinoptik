/**
 * Definition for Weather Repository singlton
 * Returns an instance of Weather Repository
 *
 * Weather Repository works with IStorage
 *
 * @param {require.js} require
 * @returns {WeatherRepository} Definition for WeatherRepository
 */
define(function (require) {
    'use strict';

    var IStorage = require('ILocalStorage');
    var IPromiseAdapter = require('IPromiseAdapter');
    var IWeather = require('IWeather');
    var _KEY_ = 'x-wether-';

    /**
     * WeatherRepository constructor
     *
     * @constructor
     * @returns {constructor} Constructor for {WeatherRepository}
     */
    function WeatherRepository() {
    }

    /**
     * Search all stored wether
     * @returns {array}
     */
    WeatherRepository.prototype.getWetherAll = function () {
        var storageData, checkDate, results, i;
        results = [];

        storageData = IStorage.searchAll(_KEY_);
        if (!storageData) {
            return [];
        }

        // Allow to store data 3 hours only
        var checkDate = new Date();
        checkDate.setHours(checkDate.getHours()-3);

        for (i in storageData) {
            this._updateStorageExpiredData(storageData[i]);
            results.push(storageData[i]);
        }
        return results;
    };

    /**
     * Get single wether by latitude and longitude
     * @returns {object|null}
     */
    WeatherRepository.prototype.getWetherById = function (id) {
        var storageRecord;
        storageRecord = IStorage.getItem(_KEY_ + id);
        if (!storageRecord) {
            return null;
        }

        this._updateStorageExpiredData(storageRecord);
        return storageRecord;
    };

    /**
     * Get single wether by latitude and longitude
     * @returns {object|null}
     */
    WeatherRepository.prototype.getWetherByCoordinates = function (latitude, longitude) {
        latitude = latitude.toFixed(0)
        longitude = longitude.toFixed(0);

        var i;

        storageData = IStorage.searchAll(_KEY_);
        if (!storageData) {
            return null;
        }

        var storageData, storageRecord;

        storageRecord = null;
        for (i in storageData) {
            if ((storageData[i].city.latitude).toFixed(0) != latitude) {
                continue;
            }
            if ((storageData[i].city.longitude).toFixed(0) != longitude) {
                continue;
            }
            this._updateStorageExpiredData(storageData[i]);
            return storageData[i];
        }

        return null;
    };

    /**
     * Set single wether by latitude and longitude
     * @returns {undefined}
     */
    WeatherRepository.prototype.setWether = function (data) {
        var storageData, storageRecord, i;

        storageData = IStorage.searchAll(_KEY_);
        storageRecord = null;
        if (storageData) {
            for (i in storageData) {
                if (storageData[i]._ID != data._ID) {
                    continue;
                }
                storageRecord = storageData[i];
                break;
            }
        }

        if (!storageRecord) {
            storageRecord = {
                '_key': _KEY_ + data._ID,
            };
        } else {
            this._updateStorageExpiredData(storageRecord);
        }
        storageRecord['updatedAt'] = new Date();
        for (i in data) {
            storageRecord[i] = data[i];
        }
        IStorage.setItem(_KEY_ + data._ID, storageRecord);
    };

    /**
     * Remove single wether by latitude and longitude
     * @returns {undefined}
     */
    WeatherRepository.prototype.removeWether = function (id) {
        IStorage.removeItem(_KEY_ + id);
    };

    /**
     * Remove single wether by latitude and longitude
     * @returns {undefined}
     */
    WeatherRepository.prototype.clear = function (id) {
        var i, storageData;
        storageData = IStorage.searchAll(_KEY_);
        for (i in storageData) {
            IStorage.removeItem(_KEY_ + storageData[i]._ID);
        }
    };

    /**
     * @returns {object} storageData
     * @returns {undefined}
     */
    WeatherRepository.prototype._updateStorageExpiredData = function (storageData) {
        var checkDate, updatedAt, wetherKey;

        // Check if the date is the same
        checkDate = new Date();
        checkDate.setHours(checkDate.getHours()-3);
        updatedAt = new Date(storageData.updatedAt);

        if (updatedAt > checkDate && updatedAt.getDate() === checkDate.getDate()) {
            return;
        }

        if (storageData.day) {
            IWeather.get1DayWeatherAsync({
                    'latitude': storageData.city.latitude,
                    'longitude': storageData.city.longitude,
                })
                .then(function (response) {
                    var i;
                    for (i in response.day) {
                        storageData.day[i] = response.day[i];
                    }
                    IStorage.setItem(storageData._key, storageData);
                });
        }

        if (storageData.list) {
            IWeather.get5DaysWeatherAsync({
                    'latitude': storageData.city.latitude,
                    'longitude': storageData.city.longitude,
                })
                .then(function (response) {
                    var i;
                    for (i in response.list) {
                        storageData.list = response.list;
                    }
                    IStorage.setItem(wetherKey, storageData);
                });
        }

    }

    // Returns an instance of WeatherRepository. This instance will be used as singlton in the application
    return new WeatherRepository();
});