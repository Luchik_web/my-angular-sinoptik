/**
 * Definition for Version Repository singlton
 * Returns an instance of Version Repository
 *
 * Version Repository works with ICombinedStorageService
 *
 * @param {requre.js} require
 * @returns {VersionRepository} Definition for VersionRepository
 */
define(function (require) {
    'use strict';

    var IStorage = require('ILocalStorage');
    var _KEY_ = 'x-web-version';

    /**
     * VersionRepository constructor
     *
     * @constructor
     * @returns {constructor} Constructor for {VersionRepository}
     */
    function VersionRepository() {
    }

    VersionRepository.prototype.getVersion = function () {
        return IStorage.getItem(_KEY_);
    };

    VersionRepository.prototype.setVersion = function (version) {
        IStorage.setItem(_KEY_, version);
    };

    VersionRepository.prototype.removeVersion = function () {
        IStorage.removeItem(_KEY_);
    };

    // Returns an instance of VersionRepository. This instance will be used as singlton in the application
    return new VersionRepository();
});