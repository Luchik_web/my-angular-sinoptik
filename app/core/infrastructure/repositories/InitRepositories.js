define(function (require) {

    var Config = require('config/Config'),
        VersionRepository = require('IVersionRepository'),
        storedVersion, newVersion;

    // Initiate Local repository data
    storedVersion = VersionRepository.getVersion();
    newVersion = Config.version;

    if (storedVersion !== newVersion) {
        VersionRepository.setVersion(newVersion);
    }
});