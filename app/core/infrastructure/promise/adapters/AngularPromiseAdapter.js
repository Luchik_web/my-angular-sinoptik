define(function (require) {
    'use strict';

    require('angular');
    var ngInjector, AngularPromiseAdapter;
    ngInjector = angular.injector(['ng']);

    AngularPromiseAdapter = ngInjector.invoke(['$q', function ($q) {
        return {
            $promiseProvider: $q,
            init: function($promiseProvider) {
                this.$promiseProvider = $promiseProvider;
            },
            defer: function () {
                var deferred = (this.$promiseProvider).defer();
                return deferred;
            },
            resolve: function (response) {
                var deferred = (this.$promiseProvider).defer();
                deferred.resolve(response);
                return deferred.promise;
            },
            reject: function (response) {
                var deferred = (this.$promiseProvider).defer();
                deferred.reject(response);
                return deferred.promise;
            }
        };
    }]);

    return AngularPromiseAdapter;
});