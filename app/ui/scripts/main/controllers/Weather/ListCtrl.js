/**
 * List of Cities
 *
 * @require presentation/main/modules/appmain
 */
define(function (require) {
    'use strict';

    // Required ANGULAR
    var angular = require('angular');
    require('presentation/main/modules/appmain');
    // Required SERVICES
    require('presentation/common/services/WeatherAppService');
    // Required FILTERS
    require('presentation/common/filters/FilterTemperature');
    require('presentation/common/filters/FilterCityAsClass');
    // Required DIRECTIVES
    require('presentation/common/directives/DrctvWeatherToday');
    require('presentation/common/directives/DrctvGeoSearch');

    angular.module('ApplicationMain.loader').controller('Weather/ListCtrl', [
        '$rootScope', '$scope', 'WeatherAppService',
        function ($rootScope, $scope, WeatherAppService) {

////////////////////////////// INITIATE DATA

            _initData();

////////////////////////////// MAIN DATA PROCESSING

////////////////////////////// SUBSCRIBE TO EVENTS

            /**
             * Reaction on 'Weather/DrctvGeoSearch' event
             */
            $scope.$on('Weather/DrctvGeoSearch', function () {
                _initData();
            });

            /**
             * Reaction on 'drctvWeatherToday/select' event
             */
            $scope.$on('drctvWeatherToday/select', function ($event, weatherObj) {
                $rootScope.selectedWetherID = weatherObj._ID;
                $rootScope.selectedWetherIsForceSelected = false;
            });

            /**
             * Reaction on 'drctvWeatherToday/delete' event
             */
            $scope.$on('drctvWeatherToday/delete', function ($event, weatherObj) {

                var weatherID;
                weatherID = weatherObj._ID;

                if (weatherID == $rootScope.selectedWetherID) {
                    $rootScope.selectedWetherID = null;
                }

                WeatherAppService.deleteWetherById(weatherID);

                var i, indexFound = null;
                for (i in $scope.weatherList) {
                    if ($scope.weatherList[i]._ID == weatherID) {
                        indexFound = i;
                        if (!$rootScope.selectedWetherID) {
                            continue;
                        }
                        break;
                    }

                    if (!$rootScope.selectedWetherID) {
                        $rootScope.selectedWetherID = $scope.weatherList[i]._ID;
                        $rootScope.selectedWetherIsForceSelected = true;
                        if (null !== indexFound) {
                            break;
                        }
                    }
                }

                if (null !== indexFound) {
                    $scope.weatherList.splice(indexFound, 1);
                }
            });

////////////////////////////// DEFINE VISIBILITY FUNCTIONALITY

////////////////////////////// DEFINE DATA PROCESSING FUNCTIONALITY

////////////////////////////// PRIVATE FUNCTIONS

            /**
             * Initiate weather list
             *
             * @returns {IPromise}
             */
            function _initData() {
                WeatherAppService.list1DayWetherByCoordinatesAsync()
                    .then(function(response) {
                        var i;
                        if (!$rootScope.selectedWetherID) {
                            for (i in response) {
                                $rootScope.selectedWetherID = response[i]._ID;
                                $rootScope.selectedWetherIsForceSelected = true;
                                break;
                            }
                        }

                        $scope.weatherList = response;
                    });
            }

////////////////////////////// END

        }
    ]);
});