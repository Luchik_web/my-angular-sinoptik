/**
 * Single city preview
 *
 * @require presentation/main/modules/appmain
 */
define(function (require) {
    'use strict';

    // Required APPLICATION
    var Application = require('presentation/main/modules/appmain');
    // Required SERVICES
    require('presentation/common/services/WeatherAppService');
    // Required FILTERS
    require('presentation/common/filters/FilterTemperature');
    require('presentation/common/filters/FilterCityAsClass');
    // Required DIRECTIVES
    require('presentation/common/directives/DrctvWeatherDayByDay');

    Application.controller('Weather/ViewCtrl', [
        '$rootScope', '$scope', 'WeatherAppService',
        function ($rootScope, $scope, WeatherAppService) {

////////////////////////////// INITIATE DATA
            $scope.currentDate = new Date();
            $scope.weatherOptions = {
                'temp' : true,
                'weekday_short' : true, //'true | false
                'weekday' : true, //'true | false
                'icon' : true, //true | false
            };

////////////////////////////// MAIN DATA PROCESSING

            $scope.deselect = function() {
                $rootScope.selectedWetherID = null;
            };

////////////////////////////// SUBSCRIBE TO EVENTS

            $rootScope.$watch('selectedWetherID', function (newValue, oldValue) {
                if (!newValue) {
                    return;
                }
                _initData();
            });

////////////////////////////// DEFINE VISIBILITY FUNCTIONALITY

////////////////////////////// DEFINE DATA PROCESSING FUNCTIONALITY

////////////////////////////// PRIVATE FUNCTIONS

            /**
             * Initiate weather list
             *
             * @returns {IPromise}
             */
            function _initData() {
                if (!$rootScope.selectedWetherID) {
                    return;
                }
                WeatherAppService.get5DaysWetherByIdAsync($rootScope.selectedWetherID)
                    .then(function (response) {
                        $scope.weatherObj = response;
                    });
            }

////////////////////////////// END

        }
    ]);
});