/**
 * Aside line
 *
 * @require presentation/main/modules/appmain
 */
define(function(require) {
    'use strict';

    // Required ANGULAR
    var angular = require('angular');
    require('presentation/main/modules/appmain');
    
    angular.module('ApplicationMain.loader').controller('_Partials/SystemPartialsCtrl', [
        function() {
        }
    ]);
});