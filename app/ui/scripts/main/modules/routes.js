/**
 * Routes definition
 *
 * @requires angular
 *
 * @param {require} require
 * @return {undefined}
 */
define(function(require) {
    'use strict';

    /**
     * Initialize all dependencies
     */
    var angular = require('angular'),
        routesList = require('presentation/main/modules/extra/routes'),
        routes;

    // Module that will be used in the application
    require('angular-ui-router');

    // Controllers that are used in routes

    /**
     * Initiate module for routes
     *
     * @return {module} new module with the {@link angular.Module} api.
     */
    routes = angular.module('ApplicationMain.routes', [
        'ui.router'
    ]);

    /**
     * Define routes for the application
     *
     * @param {$stateProvider} $stateProvider
     * @param {$urlRouterProvider} $urlRouterProvider
     */
    routes.config([
        '$stateProvider', '$urlRouterProvider', '$locationProvider',
        function($stateProvider, $urlRouterProvider, $locationProvider) {
            // Use the HTML5 History API
            // Enable routes rewrite
            $locationProvider.html5Mode(true);

            // if state is not registered in app redirect to
            $urlRouterProvider.otherwise("/");

            var i;

            for (var i in routesList) {
                $stateProvider.state(routesList[i].name, formRoute(routesList[i]));
            }

            function formRoute(routesListItem) {
                var route, i;
                route = {
                    url: routesListItem.url,
                    permissions: routesListItem.permissions ? routesListItem.permissions : [],
                };

                if (routesListItem.views) {
                    route.views = routesListItem.views;
                }

                if (routesListItem.controller) {
                    route.controller = routesListItem.controller;
                }

                if (routesListItem.templateUrl) {
                    route.templateUrl = routesListItem.templateUrl;
                }

                route.resolve = {
//                    load : ['$q', function($q) {
//                        var i, dependencies;
//
//                        dependencies = [];
//                        for (i in routesListItem.views) {
//                            if ('string' === typeof routesListItem.views[i].controller) {
//                                dependencies.push(routesListItem.views[i].controller);
//                            }
//                        }
//                        if ('string' === typeof routesListItem.controller) {
//                            dependencies.push(routesListItem.controller);
//                        }
//
//                        if (0 === dependencies.length) {
//                            return true;
//                        }
//
//                        var deferred, dependencies;
//
//                        deferred = $q.defer();
//                        require(dependencies, function() {
//                            deferred.resolve(true);
//                        }, function() {
//                            deferred.reject(arguments);
//                        });
//                        return deferred.promise;
//                    }]
                };
                
                if (routesListItem.resolve) {
                    for (i in routesListItem.resolve) {
                        route.resolve[i] = routesListItem.resolve;
                    }
                }

                return route;
            }

        }
    ]);

    return routes;

});