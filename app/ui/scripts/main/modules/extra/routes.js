/**
 * Routes definition
 *
 * @requires angular
 *
 * @param {require} require
 * @return {undefined}
 */
define(function (require) {
    'use strict';

    // Controllers that are used in routes
    var routes = [

        ////////////////////////////////
        // ANY AUTHORISED
        ////////////////////////////////

        {
            name: 'routeHome',
            url: '/',
            views: {
                aside: {
                    controller: 'Weather/ListCtrl',
                    templateUrl: 'ui/views/main/Weather/ListView.html'
                },
                main: {
                    controller: 'Weather/ViewCtrl',
                    templateUrl: 'ui/views/main/Weather/ViewView.html'
                }
            }
        },

        ////////////////////////////////
        // AUTHORISED USER
        ////////////////////////////////

        ////////////////////////////////
        // NOT AUTHORISED USER
        ////////////////////////////////

    ];
    return routes;
});