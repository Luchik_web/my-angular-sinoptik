/**
 * Application defenition
 *
 * @requires angular
 * @requires presentation/routes
 *
 * @param {require} require
 * @return {undefined}
 */
define(function(require) {
    'use strict';

    /**
     * Initialize all required dependincies
     */
    // Variables
    var application,
        angular = require('angular');

    /**
     * Initialize application
     *
     * @return {module} new module with the {@link angular.Module} api.
     */
    application = angular.module('ApplicationMain.loader' , []);
    return application;
});