/**
 * Application defenition
 *
 * @requires angular
 * @requires presentation/routes
 *
 * @param {require} require
 * @return {undefined}
 */
define(function(require) {
    'use strict';

    /**
     * Initialize all required dependincies
     */
    // Variables
    var application,
        angular = require('angular');

    // Module that will be used in the application
    require('presentation/common/modules/appcommon');
    require('presentation/main/modules/appmain');
    require('presentation/main/modules/routes');

    require('presentation/main/controllers/_Partials/SystemPartialsCtrl');
    require('presentation/main/controllers/Weather/ListCtrl');
    require('presentation/main/controllers/Weather/ViewCtrl');

    /**
     * Initialize application
     *
     * @return {module} new module with the {@link angular.Module} api.
     */
    application = angular.module('ApplicationMain' , [
        'ApplicationCommon',
        'ApplicationMain.loader',
        'ApplicationMain.routes'
    ]);

    return application;
});