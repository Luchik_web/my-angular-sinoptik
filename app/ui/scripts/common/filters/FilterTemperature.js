define(function (require) {
    'use strict';
    
    var angular = require('angular');
    require('presentation/common/modules/appcommon');

    var FilterTemperature = function ($sce) {
        return function (temperature, initialFormat, format) {
            if (!temperature) {
                return 'N/A';
            }

            initialFormat = ('string' === typeof initialFormat)
                ? initialFormat.toUpperCase()
                : 'C';

            var value;
            switch (initialFormat) {
                // Kelvin
                case 'KELVIN':
                case 'K':
                    value = temperature - 273.15;
                    break;
                    // Farengate
                case 'FARENGATE':
                case 'F':
                    value = (temperature - 32) * 5 / 9;
                    break;
                    // Celsius
                default:
                    value = temperature;
                    break;
            }
            
            if (format) {
                return value.toFixed(0);
            } else {
                return $sce.trustAsHtml(value.toFixed(0) + '<sup>o</sup>');
            }
        };
    };
    angular.module('ApplicationCommon').filter('FilterTemperature', ['$sce', FilterTemperature]);
});