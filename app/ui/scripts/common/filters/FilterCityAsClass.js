define(function (require) {
    'use strict';

    var angular = require('angular');
    require('presentation/common/modules/appcommon');
    
    var FilterCityAsClass = function () {
        return function (cityName, base) {
            cityName = ('string' === typeof cityName)
                ? cityName.toLowerCase()
                : '';
            base = ('string' === typeof base)
                ? base
                : '';

            if (!cityName) {
                return '';
            }

            cityName = cityName.replace(/ /gi, '-');
            return base + cityName;
        };
    };
    angular.module('ApplicationCommon').filter('FilterCityAsClass', [FilterCityAsClass]);

    return FilterCityAsClass;
});