/**
 * Application module for locale
 *
 * @requires angular
 *
 * @param {require} require
 * @return {module} new module with the {@link angular.Module} api.
 */
define(function(require) {
    'use strict';

    /**
     * Initialize all dependencies
     */
    // Additional dependencies
    var ILocaleRepository = require('ILocaleRepository');
    // Required APPLICATION
    var angular = require('angular'),
        locale,
        Config = require('config/Config');

    // Module that will be used in the application
    require('translate');

    /**
     * Initiate module for locale
     *
     * @return {module} new module with the {@link angular.Module} api.
     */
    locale = angular.module('ApplicationCommon.locale', [
        'gettext'
    ]);

    /**
     * Initiate firs run of the defined application
     */
    locale.run(function(gettextCatalog) {
        var locale;
        locale = ILocaleRepository.getLocale();

        if (null === locale) {
            locale = Config.defaultLocale;
            ILocaleRepository.setLocale(locale);
        }
        gettextCatalog.currentLanguage = locale;
    });

    return locale;
});