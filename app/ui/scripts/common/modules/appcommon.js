/**
 * Application defenition
 *
 * @requires angular
 * @requires presentation/routes
 *
 * @param {require} require
 * @return {undefined}
 */
define(function(require) {
    'use strict';

    /**
     * Initialize all required dependincies
     */
    // Variables
    var application,
        angular = require('angular'),
        IHttpAdapter = require('IHttpAdapter'),
        IPromiseAdapter = require('IPromiseAdapter');

    // Module that will be used in the application
    require('presentation/common/modules/ui');
    require('presentation/common/modules/locale');
    require('templates');

    /**
     * Initialize application
     *
     * @return {module} new module with the {@link angular.Module} api.
     */
    application = angular.module('ApplicationCommon' , [
        'ApplicationCommon.ui',
        'ApplicationCommon.templates',
        'ApplicationCommon.locale'
    ]);

    /**
     * Initiate firs run of the defined application
     */
    application.run(function($http, $q) {
        IPromiseAdapter.init($q);
        IHttpAdapter.init($http);
    });

    return application;
});