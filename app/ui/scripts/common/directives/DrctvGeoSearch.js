define(function (require) {
    'use strict';

    // Required MODULE
    var angular = require('angular');
    require('presentation/common/modules/appcommon');
    // Required SERVICES
    require('presentation/common/services/WeatherAppService');

    angular.module('ApplicationCommon').directive('drctvGeoSearch', [
        'WeatherAppService',
        function (WeatherAppService) {
            return {
                // can be used as attribute and class
                restrict: 'AC',
                link: function ($scope, el, attrs) {
                    var input = angular.element(el)[0];

                    var autocomplete = new google.maps.places.Autocomplete(input);
                    autocomplete.setTypes(['(regions)']); //[] , ['address'], ['establishment'], ['geocode']

                    autocomplete.addListener('place_changed', function () {
                        var place = autocomplete.getPlace();
                        if (!place.geometry) {
                            // User entered the name of a Place that was not suggested and
                            // pressed the Enter key, or the Place Details request failed.
                            window.alert("No details available for input: '" + place.name + "'");
                            return;
                        }

                        var latitude, longitude;
                        latitude = ('function' === typeof place.geometry.location.lat)
                            ? place.geometry.location.lat()
                            : place.geometry.location.lat;
                        longitude = ('function' === typeof place.geometry.location.lng)
                            ? place.geometry.location.lng()
                            : place.geometry.location.lng;

                        WeatherAppService.get1DayWetherByCoordinatesAsync(latitude, longitude)
                            .then(function (response) {
                                $scope.$broadcast('Weather/DrctvGeoSearch', null);
                            });
                    });
                }
            };

        }
    ]);
});