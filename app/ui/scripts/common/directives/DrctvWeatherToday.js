define(function(require) {
    'use strict';

    // Required MODULE
    var angular = require('angular');
    require('presentation/common/modules/appcommon');
    require('presentation/common/services/WeatherAppService');
    // Required FILTERS
    require('presentation/common/filters/FilterTemperature');

    angular.module('ApplicationCommon').directive('drctvWeatherToday', [
        'WeatherAppService',
        function(WeatherAppService) {
            return {
                // can be used as attribute and element
                restrict: 'AE',
                // Close scope
                scope: {
                    'weatherObj': '=',
                },
                // Define template
                templateUrl: 'ui/views/common/Directive/DrctvWeatherToday.html',
                // Define controller
                controller: function($scope) {
                    $scope.select = function(weatherObj) {
                        $scope.$emit('drctvWeatherToday/select', weatherObj);
                    };
                    
                    $scope.delete = function(weatherObj) {
                        $scope.$emit('drctvWeatherToday/delete', weatherObj);
                    };
                },
            };

        }
    ]);
});