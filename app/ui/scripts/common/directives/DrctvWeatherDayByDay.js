define(function(require) {
    'use strict';

    // Required MODULE
    var angular = require('angular');
    require('presentation/common/modules/appcommon');
    require('presentation/common/services/WeatherAppService');
    // Required FILTERS
    require('presentation/common/filters/FilterTemperature');

    angular.module('ApplicationCommon').directive('drctvWeatherDayByDay', [
        'WeatherAppService',
        function(WeatherAppService) {
            var _defaultOptions = {
                'temp' : true,
                'location' : false, //true | false
                'weekday_short' : true, //true | false
                'weekday_long' : false, //true | false
                'day' : false, //true | false
                'moth' : false, //true | false
                'description' : false, //true | false
                'icon' : true, //true | false
            };
            
            return {
                // can be used as attribute and element
                restrict: 'AE',
                // Close scope
                scope: {
                    'weatherObj': '=',
                    'options': '=',
                    'day': '=',
                },
                // Define template
                templateUrl: 'ui/views/common/Directive/DrctvWeatherDayByDay.html',
                // Define controller
                link: function($scope, el, attrs) {
                    $scope.weatherDayObj = {};
                    Object.assign($scope.options, _defaultOptions, $scope.options)    
                    
                        
                    $scope.weatherDayObj = {};
                    var list, i, todayDay, listDate, listDay, todayDate, selection;
                    list = $scope.weatherObj.list;

                    todayDate = new Date();

                    todayDay = todayDate.getDate() + ($scope.day - 1);
                    todayDate = new Date(todayDate.setDate(todayDay));

                    for (i in list) {
                        if (!selection) {
                            selection = i;
                        }
                        listDate = new Date(list[i].date);
                        listDay = listDate.getDate();

                        if (listDay < todayDay) {
                            selection = i;
                            continue;
                        } else if (listDay > todayDay) {
                            break;
                        }

                        if (todayDate < listDate) {
                            selection = i;
                            break;
                        }
                        todayDate = listDate;
                        selection = i;
                    }
                    $scope.weatherDayObj = list[selection];

                },
            };

        }
    ]);
});