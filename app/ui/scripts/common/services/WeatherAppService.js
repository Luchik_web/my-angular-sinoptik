define(function(require) {
    'use strict';

    var angular = require('angular'),
        WeatherAppService = require('appservices/WeatherAppService');
    require('presentation/common/modules/appcommon');

    angular.module('ApplicationCommon').service('WeatherAppService', [
        function() {
            return WeatherAppService;
        }
    ]);
});