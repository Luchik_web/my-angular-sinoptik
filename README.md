AngularJS by Example
========================================

An application to demonstrate work with AngularJS

Used css compiler - less


Requirements
========================================

Requirements:
1. nodejs
2. gulp
3. bower

If you have the node package manager, npm, installed you will also need 
1. coffee-script
```shell
npm install -g coffee-script
```
2. Make sure you have gulp installed globally (`npm install -g gulp`)
```shell
npm install -g coffee-script
```


Getting started
========================================

1. Clone repo
2. Create your own config file named
```
/app/config/Config.js
```
from template
```
/app/config/Config.js.tmpl
```
3. Run commands
```shell
bower install
gulp compile
```
4. To run test server execute commands
```shell
npm install
node server
```


Testing
========================================

Run command
```shell
gulp karma
```

Test results will be rendered to folder
```
/build/tests/unit/html/...
```


Current states and future goalas
========================================

Initial data selection is based on your location. Countrie selection is based on google search

1. Increase test coverege
* Test coverege must be increased. 
* Core files need to be covered with unit test

2. Wether object need to be more structurised. Current wether API need to be covered with e2e tests to 
be sure that we will alwasy keep current structure in proper state

3. Change icons. 
Need to gether all possible icons for wether to create base for changing layout 

4. Allow user to define urls for cities the user selected

5. Allow user to select range for weather he want to see

5. Use minimization / uglification for css/js files