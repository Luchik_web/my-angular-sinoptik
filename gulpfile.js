var gulp = require('gulp');
var less = require('gulp-less');
var path = require('path');
var minifyCSS = require('gulp-minify-css');
var rename = require('gulp-rename');
var clean = require('gulp-clean');
var replace = require('gulp-replace');
var gettext = require('gulp-angular-gettext');

// ----------------------------------------------------------------------

var Server = require('karma').Server;

// ---------- SETTINGS --------------------------------------------------

var _enviroment = 'dev';
var lessPath = './app/ui/styles/less';
var cssPath = './app/ui/styles/css';

var viewPath = './app/ui/view';
var scriptPath = './app/ui/scripts';
var poPath = './app/ui/po';

// ---------- TASKS --------------------------------------------------

gulp.task('default', function() {
    console.log('Generation of css [' + cssPath + '/] from less [' + lessPath + '].'
        + '\n Commands : '
        + '\n Run tests: \n    gulp karma'
        + '\n Compile css from less once: \n    gulp compile'
        + '\n Background watch less: \n    gulp watch');
});

/**
 * Watch css/html
 */
gulp.task('compile', function() {
    return _lessHelper(['styles']);
});

/**
 * Watch css/html
 */
gulp.task('watch', ['compile'], function() {
    gulp.watch([lessPath + '/**/*.less'], ['compile']);
});

gulp.task('pot', function () {
    return gulp.src([viewPath + '/**/*.html', scriptPath + '/**/*.js'])
        .pipe(gettext.extract('template.pot', {
            // options to pass to angular-gettext-tools... 
        }))
        .pipe(gulp.dest(poPath + '/'));
});
 
gulp.task('translations', function () {
    return gulp.src(poPath + '/**/*.po')
        .pipe(gettext.compile({
            // options to pass to angular-gettext-tools... 
            format: 'json'
        }))
        .pipe(gulp.dest(scriptPath + '/common/modules/extra/'));
});

gulp.task('karma', function (done) {
  new Server({
    configFile: __dirname + '/karma.conf.js',
    singleRun: true,
    autoWatch: false
  }, done).start();
});
// ---------- PROCESS CSS --------------------------------------------------

function _lessHelper(fname) {
    fname = fname || '**/*';
    return gulp.src(lessPath + '/' + fname + '.less')
        .pipe(less({
            paths: [path.join(__dirname, 'less', 'includes')]
        }))
        .pipe(gulp.dest(cssPath))
        .pipe(minifyCSS())
        .pipe(replace('http://', '//'))
        .pipe(rename({extname: '.css'})) //rename minified file: .pipe(rename({extname: '.min.css'}))
        .pipe(gulp.dest(cssPath));
}