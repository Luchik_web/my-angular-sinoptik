define(function (require) {
    'use strict';

    var IPromiseAdapter = require('IPromiseAdapter');

    /**
     * -----------------------------------------------------------------------------------------------------------------
     * Constructor for OpenWeatherMap
     * -----------------------------------------------------------------------------------------------------------------
     *
     * @public
     * @constructor
     * @returns {undefined}
     */

    function OpenWeatherMap()
    {
    }

    /**
     * Get 1 day wether
     * @retur {Promise<object>}
     */
    OpenWeatherMap.prototype.get1DayWeatherAsync = function (params)
    {
        return IPromiseAdapter.resolve({
                _key: 'x-wether-2643743',
                updatedAt: new Date(),
                _ID: 2643743,
                "city":{"name":"London","country":"GB","latitude":51.5085,"longitude":-0.1258},
                "coord":{"lon":-0.1258,"lat":51.5085},
                "day":{"date":date1,
                    "main":{"temp":290.37,"pressure":1021,
                        "humidity":88,"temp_min":289.15,"temp_max":291.15},
                    "weather":[{"id":701,"main":"Mist","description":"mist","icon":"50n"}],
                    "wind":{"speed":2.6,"deg":90},
                    "clouds":{"all":90}},
                "list":[
                    {"date":date1,
                        "main":{"temp":290.45,"humidity":84,"temp_max":290.45,"temp_min":288.56},
                        "weather":[{"id":803,"main":"Clouds","description":"broken clouds","icon":"04n"}],
                        "clouds":64},
                    {"date":date2,
                        "main":{"temp":295.12,"humidity":78,"temp_max":296.43,"temp_min":288.01},
                        "weather":[{"id":803,"main":"Clouds","description":"broken clouds","icon":"04d"}],
                        "clouds":64},
                    {"date":date3,"main":{"temp":294.13,
                            "humidity":68,"temp_max":294.14,"temp_min":289.82},"weather":[{"id":802,
                                "main":"Clouds","description":"scattered clouds","icon":"03d"}],"clouds":32},
                    {"date":date4,"main":{"temp":293.42,"humidity":63,"temp_max":294.35,
                            "temp_min":287.37},"weather":[{"id":803,"main":"Clouds","description":"broken clouds","icon":"04d"}],
                        "clouds":76},
                    {"date":date5,"main":{"temp":294.63,"humidity":0,"temp_max":294.63,
                            "temp_min":283.92},"weather":[{"id":800,"main":"Clear","description":"sky is clear","icon":"01d"}],
                        "clouds":40},
                    {"date":date6,"main":{"temp":295.86,"humidity":0,
                            "temp_max":295.86,"temp_min":285.2},"weather":[{"id":800,"main":"Clear",
                                "description":"sky is clear","icon":"01d"}],"clouds":58},
                    {"date":date7,"main":{"temp":292.11,"humidity":0,"temp_max":292.11,"temp_min":286.55},
                        "weather":[{"id":800,"main":"Clear","description":"sky is clear","icon":"01d"}],"clouds":54}]
            });
    };

    /**
     * Get 5 days wether
     *
     * @retur {Promise<object>}
     */
    OpenWeatherMap.prototype.get5DaysWeatherAsync = function (params)
    {
        return IPromiseAdapter.resolve({
                _key: 'x-wether-2643743',
                updatedAt: new Date(),
                _ID: 2643743,
                "city":{"name":"London","country":"GB","latitude":51.5085,"longitude":-0.1258},
                "coord":{"lon":-0.1258,"lat":51.5085},
                "day":{"date":date1,
                    "main":{"temp":290.37,"pressure":1021,
                        "humidity":88,"temp_min":289.15,"temp_max":291.15},
                    "weather":[{"id":701,"main":"Mist","description":"mist","icon":"50n"}],
                    "wind":{"speed":2.6,"deg":90},
                    "clouds":{"all":90}},
                "list":[
                    {"date":date1,
                        "main":{"temp":290.45,"humidity":84,"temp_max":290.45,"temp_min":288.56},
                        "weather":[{"id":803,"main":"Clouds","description":"broken clouds","icon":"04n"}],
                        "clouds":64},
                    {"date":date2,
                        "main":{"temp":295.12,"humidity":78,"temp_max":296.43,"temp_min":288.01},
                        "weather":[{"id":803,"main":"Clouds","description":"broken clouds","icon":"04d"}],
                        "clouds":64},
                    {"date":date3,"main":{"temp":294.13,
                            "humidity":68,"temp_max":294.14,"temp_min":289.82},"weather":[{"id":802,
                                "main":"Clouds","description":"scattered clouds","icon":"03d"}],"clouds":32},
                    {"date":date4,"main":{"temp":293.42,"humidity":63,"temp_max":294.35,
                            "temp_min":287.37},"weather":[{"id":803,"main":"Clouds","description":"broken clouds","icon":"04d"}],
                        "clouds":76},
                    {"date":date5,"main":{"temp":294.63,"humidity":0,"temp_max":294.63,
                            "temp_min":283.92},"weather":[{"id":800,"main":"Clear","description":"sky is clear","icon":"01d"}],
                        "clouds":40},
                    {"date":date6,"main":{"temp":295.86,"humidity":0,
                            "temp_max":295.86,"temp_min":285.2},"weather":[{"id":800,"main":"Clear",
                                "description":"sky is clear","icon":"01d"}],"clouds":58},
                    {"date":date7,"main":{"temp":292.11,"humidity":0,"temp_max":292.11,"temp_min":286.55},
                        "weather":[{"id":800,"main":"Clear","description":"sky is clear","icon":"01d"}],"clouds":54}]
            });
    };

    /**
     * Get 7 days wether
     *
     * @retur {Promise<object>}
     */
    OpenWeatherMap.prototype.get7DaysWeatherAsync = function (params)
    {
        return IPromiseAdapter.resolve({
                _key: 'x-wether-2643743',
                updatedAt: new Date(),
                _ID: 2643743,
                "city":{"name":"London","country":"GB","latitude":51.5085,"longitude":-0.1258},
                "coord":{"lon":-0.1258,"lat":51.5085},
                "day":{"date":date1,
                    "main":{"temp":290.37,"pressure":1021,
                        "humidity":88,"temp_min":289.15,"temp_max":291.15},
                    "weather":[{"id":701,"main":"Mist","description":"mist","icon":"50n"}],
                    "wind":{"speed":2.6,"deg":90},
                    "clouds":{"all":90}},
                "list":[
                    {"date":date1,
                        "main":{"temp":290.45,"humidity":84,"temp_max":290.45,"temp_min":288.56},
                        "weather":[{"id":803,"main":"Clouds","description":"broken clouds","icon":"04n"}],
                        "clouds":64},
                    {"date":date2,
                        "main":{"temp":295.12,"humidity":78,"temp_max":296.43,"temp_min":288.01},
                        "weather":[{"id":803,"main":"Clouds","description":"broken clouds","icon":"04d"}],
                        "clouds":64},
                    {"date":date3,"main":{"temp":294.13,
                            "humidity":68,"temp_max":294.14,"temp_min":289.82},"weather":[{"id":802,
                                "main":"Clouds","description":"scattered clouds","icon":"03d"}],"clouds":32},
                    {"date":date4,"main":{"temp":293.42,"humidity":63,"temp_max":294.35,
                            "temp_min":287.37},"weather":[{"id":803,"main":"Clouds","description":"broken clouds","icon":"04d"}],
                        "clouds":76},
                    {"date":date5,"main":{"temp":294.63,"humidity":0,"temp_max":294.63,
                            "temp_min":283.92},"weather":[{"id":800,"main":"Clear","description":"sky is clear","icon":"01d"}],
                        "clouds":40},
                    {"date":date6,"main":{"temp":295.86,"humidity":0,
                            "temp_max":295.86,"temp_min":285.2},"weather":[{"id":800,"main":"Clear",
                                "description":"sky is clear","icon":"01d"}],"clouds":58},
                    {"date":date7,"main":{"temp":292.11,"humidity":0,"temp_max":292.11,"temp_min":286.55},
                        "weather":[{"id":800,"main":"Clear","description":"sky is clear","icon":"01d"}],"clouds":54}]
            });
    };

    // Returns an instance of OpenWeatherMap. This instance will be used as singlton in the applicatio
    return new OpenWeatherMap();
});
