define(function (require) {
    'use strict';

    require('presentation/common/modules/ui');

    describe('CM3. On demand ApplicationCommon.ui module loaded and initialized correctly', function () {
        var appModule, appDependencies, i, examplesScenario2;

        beforeEach(function () {
            appModule = angular.module("ApplicationCommon.ui");
            //  list/array of modules which the injector will load before the app module is loaded
            appDependencies = appModule.requires;
        });

        //  test if the application module has been initialized correctly

        // [*] --- Start Scenario 1 START ---
        // [*] System is running
        // [*] When ApplicationCommon.ui is loaded
        // [*] Then ApplicationCommon.ui initialized
        it("Scenario 1. ApplicationCommon.ui module initialized", function () {
            expect(appModule).toBeTruthy();
        });
        // [*] --- End Scenario 1 START ---

        //  test if the application module has arr requreed dependencies
        // [*] --- Start Scenario 2 START ---
        // [*] System is running
        // [*] When ApplicationCommon.ui is loaded
        // [*] Then ApplicationCommon.ui includes required dependency
        // [*] Examples:
        examplesScenario2 = [
            ['ui.bootstrap'],
            ['angularMoment'],
        ];
        for (i in examplesScenario2) {
            (function (moduleName) {
                it('Scenario 2. ApplicationCommon.ui module has ' + moduleName + ' as a dependency', function () {
                    expect(appDependencies.indexOf(moduleName) > -1).toBeTruthy();
                });
            })(examplesScenario2[i][0])
        }
        // [*] --- End Scenario 2 START ---
    });
});