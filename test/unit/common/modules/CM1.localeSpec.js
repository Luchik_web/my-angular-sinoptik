define(function(require) {
    'use strict';

    require('presentation/common/modules/locale');

    describe('CM1. On demand ApplicationCommon.locale module loaded and initialized correctly', function() {
        var appModule, appDependencies, i, examplesScenario2;
        beforeEach(function() {
            appModule = angular.module("ApplicationCommon.locale");
            //  list/array of modules which the injector will load before the app module is loaded
            appDependencies = appModule.requires;
        });

        //  test if the application module has been initialized correctly
        // [*] --- Start Scenario 1 START ---
        // [*] System is running
        // [*] When ApplicationCommon.locale is loaded
        // [*] Then ApplicationCommon.locale initialized
        it("Scenario 1. ApplicationCommon.locale module initialized", function() {
            expect(appModule).toBeTruthy();
        });
        // [*] --- End Scenario 1 START ---

        //  test if the application module has arr requreed dependencies
        // [*] --- Start Scenario 2 START ---
        // [*] System is running
        // [*] When ApplicationCommon.locale is loaded
        // [*] Then ApplicationCommon.locale includes required dependency
        // [*] Examples:
        examplesScenario2 = [
            ['gettext']
        ];
        for (i in examplesScenario2) {
            (function(moduleName) {
                it('Scenario 2. ApplicationCommon.locale module has ' + moduleName + ' as a dependency', function() {
                    expect(appDependencies.indexOf(moduleName) > -1).toBeTruthy();
                });
            })(examplesScenario2[i][0])
        }
        // [*] --- End Scenario 2 START ---
    });
});