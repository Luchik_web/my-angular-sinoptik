define(function(require) {
    'use strict';

    require('presentation/common/modules/templates');

    describe('CM2. On demand ApplicationCommon.templates module loaded and initialized correctly', function() {
        var appModule, appDependencies;
        beforeEach(function() {
            appModule = angular.module("ApplicationCommon.templates");
            //  list/array of modules which the injector will load before the app module is loaded
            appDependencies = appModule.requires;
        });

        //  test if the application module has been initialized correctly

        // [*] --- Start Scenario 1 START ---
        // [*] System is running
        // [*] When ApplicationCommon.templates is loaded
        // [*] Then ApplicationCommon.templates initialized
        it("Scenario 1. ApplicationCommon.templates module initialized", function() {
            expect(appModule).toBeTruthy();
        });
        // [*] --- End Scenario 1 START ---
    });
});