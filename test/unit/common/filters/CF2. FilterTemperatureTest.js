define(function (require) {
    'use strict';

    require('presentation/common/filters/FilterTemperature');

    describe('CF2. Check if FilterTemperature filter returns the expected value', function () {
        var $filter, i, examplesScenario1;

        beforeEach(module('ApplicationCommon'));

        beforeEach(inject(function (_$filter_) {
            $filter = _$filter_;

        }));

        // [*] --- Start Scenario 1 START ---
        // [*] System is running
        // [*] When system needs to apply FilterTemperature filter
        // [*]  And temperature is set to <temperature>
        // [*]  And initial format is set to <initialFormat>
        // [*] Then FilterTemperature filter is applied
        // [*]  And result value is <expected>
        // [*] Examples:
        examplesScenario1 = [
            {temperature : 310, initialFormat: 'KELVIN', expected: '37'},
            {temperature : 310, initialFormat: 'k', expected: '37'},
            {temperature : 212, initialFormat: 'FArENGaTE', expected: '100'},
            {temperature : 212, initialFormat: 'F', expected: '100'},
            {temperature : -34, initialFormat: '', expected: '-34'},
        ];
        for (i in examplesScenario1) {
            (function (temperature, initialFormat, expected) {
                it('Scenario 1. FilterTemperature appliedtp city <' + temperature + '> and base <' + initialFormat + '>', function () {
                    var result;
                    result = $filter('FilterTemperature')(temperature, initialFormat, 1);
                    expect(result).toEqual(expected);
                });
            })(examplesScenario1[i].temperature, examplesScenario1[i].initialFormat, examplesScenario1[i].expected)
        }
        // [*] --- End Scenario 1 START ---

    });
});