define(function (require) {
    'use strict';

    require('presentation/common/filters/FilterCityAsClass');

    describe('CF1. Check if FilterCityAsClass filter returns the expected value', function () {
        var $filter, i, examplesScenario1;

        beforeEach(module('ApplicationCommon'));

        beforeEach(inject(function (_$filter_) {
            $filter = _$filter_;

        }));

        // [*] --- Start Scenario 2 START ---
        // [*] System is running
        // [*] When system needs to apply FilterCityAsClass filter
        // [*]  And city is set to <city>
        // [*]  And base is set to <base>
        // [*] Then FilterCityAsClass filter is applied
        // [*]  And result value is <expected>
        // [*] Examples:
        examplesScenario1 = [
            {city: 'Los Angeles', base: 'some_base__', expected: 'some_base__los-angeles'},
            {city: 'City Of LonDon', base: '', expected: 'city-of-london'}
        ];
        for (i in examplesScenario1) {
            (function (city, base, expected) {
                it('Scenario 1. FilterCityAsClass appliedtp city <' + city + '> and base <' + base + '>', function () {
                    var result;
                    result = $filter('FilterCityAsClass')(city, base);
                    // Assert.
                    expect(result).toEqual(expected);
                });
            })(examplesScenario1[i].city, examplesScenario1[i].base, examplesScenario1[i].expected)
        }
        // [*] --- End Scenario 2 START ---

    });
});