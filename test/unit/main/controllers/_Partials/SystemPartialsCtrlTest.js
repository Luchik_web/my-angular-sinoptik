define(function (require) {
    'use strict';

    require('presentation/main/controllers/_Partials/SystemPartialsCtrl');

    describe('_Partials/SystemPartialsCtrl controller test', function () {
        var $controller;

        beforeEach(module('ApplicationMain.loader'));

        beforeEach(inject(function(_$controller_) {
            $controller = _$controller_;
        }));

        it('Controller _Partials/SystemPartialsCtrl exsits for module ApplicationMain.loader', function () {
            var controller, $scope;
            $scope = {};
            controller = $controller('_Partials/SystemPartialsCtrl', { $scope: $scope });
            expect(controller).toBeDefined();
        });
    });
});