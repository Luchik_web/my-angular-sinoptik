define(function (require) {
    'use strict';

    var WeatherRepositoryHelper = require('testhelpers/WeatherRepositoryHelper');
    var IPromiseAdapter = require('IPromiseAdapter');
    require('presentation/main/controllers/Weather/ViewCtrl');
    var WeatherAppService = require('appservices/WeatherAppService');

    describe('MC2. Weather/ViewCtrl controller test', function () {
        var controller, $scope, $rootScope, inRepoDataCount, selectedWetherID;


        beforeEach(module('ApplicationMain.loader'));

        beforeEach(function () {
            inRepoDataCount = 6;
            WeatherRepositoryHelper.init(inRepoDataCount);
            selectedWetherID = WeatherRepositoryHelper.getID(3);
        });


        beforeEach(inject(function($controller, _$rootScope_, _$q_) {
            IPromiseAdapter.init(_$q_);
            $scope = _$rootScope_.$new();
            $rootScope = _$rootScope_;

            $rootScope.selectedWetherID = selectedWetherID;
            controller = $controller('Weather/ViewCtrl', {
                $rootScope: $rootScope, $scope: $scope,
                WeatherAppService: WeatherAppService});
            $scope.$digest();
            $rootScope.$digest();
        }));

        it('Scenario 1. Controller Weather/ViewCtrl exsits for module ApplicationMain.loader', function () {
            expect(controller).toBeDefined();
        });

        it('Scenario 2. Data are loaded for selected Wether ID predefined in $rootScope', function (done) {
            // should set the timeout of this test to 1000 ms; instead will fail
            setTimeout(function () {
                expect($scope.weatherObj).toBeDefined();
                expect($scope.weatherObj._ID * 1).toEqual(selectedWetherID);
                done();
            }, 1000);
        });
    });
});