define(function (require) {
    'use strict';

    var WeatherRepositoryHelper = require('testhelpers/WeatherRepositoryHelper');
    var IPromiseAdapter = require('IPromiseAdapter');
    require('presentation/main/controllers/Weather/ListCtrl');
    var WeatherAppService = require('appservices/WeatherAppService');

    describe('MC1. Weather/ListCtrl controller test', function () {
        var controller, $scope, $rootScope, inRepoDataCount;
        
        beforeEach(module('ApplicationMain.loader'));

        beforeEach(function () {
            inRepoDataCount = 6;
            WeatherRepositoryHelper.init(inRepoDataCount);
        });


        beforeEach(inject(function($controller, _$rootScope_, _$q_) {
            IPromiseAdapter.init(_$q_);
            $scope = _$rootScope_.$new();
            $rootScope = _$rootScope_;
            controller = $controller('Weather/ListCtrl', {
                $rootScope: $rootScope, $scope: $scope,
                WeatherAppService: WeatherAppService});
            $scope.$digest();
        }));

        it('Scenario 1. Controller Weather/ListCtrl exsits for module ApplicationMain.loader', function () {
            expect(controller).toBeDefined();
        });

        it('Scenario 2. Initial wether list exists', function (done) {
            // should set the timeout of this test to 1000 ms; instead will fail
            setTimeout(function () {
                expect(($scope.weatherList).length).toEqual(inRepoDataCount);
                done();
            }, 1000);
        });

        it('Scenario 3. $rootScope values for initial selection are set in Weather/ListCtrl', function (done) {
            // should set the timeout of this test to 1000 ms; instead will fail
            setTimeout(function () {
                expect($rootScope.selectedWetherIsForceSelected).toEqual(true);
                expect(1 * $rootScope.selectedWetherID).toEqual(WeatherRepositoryHelper.firstID());
                done();
            }, 1000);
        });
    });
});