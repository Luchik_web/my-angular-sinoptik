define(function(require) {
    'use strict';

    require('presentation/main/modules/appmain');

    describe('MM1. On demand ApplicationMain.loader module loaded and initialized correctly', function() {
        var appModule, appDependencies;
        beforeEach(function() {
            appModule = angular.module("ApplicationMain.loader");
            //  list/array of modules which the injector will load before the app module is loaded
            appDependencies = appModule.requires;
        });

        //  test if the application module has been initialized correctly
        // [*] --- Start Scenario 1 START ---
        // [*] System is running
        // [*] When ApplicationMain.loader is loaded
        // [*] Then ApplicationMain.loader initialized
        it("Scenario 1. AngularSuperhero module initialized", function() {
            expect(appModule).toBeTruthy();
        });
        // [*] --- End Scenario 1 START ---
    });
});