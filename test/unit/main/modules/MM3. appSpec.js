define(function(require) {
    'use strict';

    require('presentation/main/modules/app');

    describe('MM3. On demand ApplicationMain module loaded and initialized correctly', function() {
        var appModule, appDependencies, i, examplesScenario2;

        beforeEach(function() {
            appModule = angular.module("ApplicationMain");
            //  list/array of modules which the injector will load before the app module is loaded
            appDependencies = appModule.requires;
        });

        //  test if the application module has been initialized correctly

        // [*] --- Start Scenario 1 START ---
        // [*] System is running
        // [*] When ApplicationMain.routes is loaded
        // [*] Then ApplicationMain.routes initialized
        it("Scenario 1. ApplicationMain module initialized", function() {
            expect(appModule).toBeTruthy();
        });
        // [*] --- End Scenario 1 START ---

        //  test if the application module has arr requreed dependencies
        // [*] --- Start Scenario 2 START ---
        // [*] System is running
        // [*] When ApplicationMain is loaded
        // [*] Then ApplicationMain includes required dependency
        // [*] Examples:
        examplesScenario2 = [
            ['ApplicationMain.loader'],
            ['ApplicationMain.routes'],
            ['ApplicationCommon'],
        ];
        for (i in examplesScenario2) {
            (function (moduleName) {
                it('Scenario 2. ApplicationMain has ' + moduleName + ' as a dependency', function () {
                    expect(appDependencies.indexOf(moduleName) > -1).toBeTruthy();
                });
            })(examplesScenario2[i][0])
        }
        // [*] --- End Scenario 2 START ---
    });
});