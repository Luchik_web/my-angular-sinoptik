define(function (require) {
    'use strict';

    require('presentation/main/modules/routes');

    describe('MM2. On demand ApplicationMain.routes module loaded and initialized correctly', function () {
        var appModule, appDependencies, i, examplesScenario2;

        beforeEach(function () {
            appModule = angular.module("ApplicationMain.routes");
            //  list/array of modules which the injector will load before the app module is loaded
            appDependencies = appModule.requires;
        });

        //  test if the application module has been initialized correctly

        // [*] --- Start Scenario 1 START ---
        // [*] System is running
        // [*] When ApplicationMain.routes is loaded
        // [*] Then ApplicationMain.routes initialized
        it("Scenario 1. ApplicationMain.routes module initialized", function () {
            expect(appModule).toBeTruthy();
        });
        // [*] --- End Scenario 1 START ---

        //  test if the application module has arr requreed dependencies
        // [*] --- Start Scenario 2 START ---
        // [*] System is running
        // [*] When ApplicationMain.routes is loaded
        // [*] Then ApplicationMain.routes includes required dependency
        // [*] Examples:
        examplesScenario2 = [
            ['ui.router']
        ];
        for (i in examplesScenario2) {
            (function (moduleName) {
                it('Scenario 2. ApplicationMain.routes has ' + moduleName + ' as a dependency', function () {
                    expect(appDependencies.indexOf(moduleName) > -1).toBeTruthy();
                });
            })(examplesScenario2[i][0])
        }
        // [*] --- End Scenario 2 START ---
    });
});