define(function (require) {
    'use strict';

    var IWeatherRepository = require('IWeatherRepository');
    var WeatherRepositoryHelper = {
        firstID: function() {
            var i,weatherData;
            weatherData = IWeatherRepository.getWetherAll();
            for (i in weatherData) {
                return weatherData[i]._ID;
            }
            return null;
        },
        getID: function(index) {
            var i, count, weatherData;
            weatherData = IWeatherRepository.getWetherAll();
            
            count = 0;
            for (i in weatherData) {
                if (count === index) {
                    return weatherData[i]._ID;
                }
                count++;
            }
            return null;
        },
        init: function(count) {
            var i, currentID;
            currentID = 2643742;
           
            IWeatherRepository.clear();
            this.ids = [];
            
            for (i = 0; i < count; i++) {
                
                var date1 = new Date();
                var date2 = new Date(date1.setDate(date1.getDate() + 1));
                var date3 = new Date(date1.setDate(date1.getDate() + 2));
                var date4 = new Date(date1.setDate(date1.getDate() + 3));
                var date5 = new Date(date1.setDate(date1.getDate() + 4));
                var date6 = new Date(date1.setDate(date1.getDate() + 5));
                var date7 = new Date(date1.setDate(date1.getDate() + 6));

                IWeatherRepository.setWether({
                    _key: 'x-wether-' + currentID,
                    updatedAt: new Date(),
                    _ID: currentID,
                    "city": {"name": "London", "country": "GB", "latitude": 51.5085, "longitude": -0.1258},
                    "coord": {"lon": -0.1258, "lat": 51.5085},
                    "day": {"date": date1,
                        "main": {"temp": 290.37, "pressure": 1021,
                            "humidity": 88, "temp_min": 289.15, "temp_max": 291.15},
                        "weather": [{"id": 701, "main": "Mist", "description": "mist", "icon": "50n"}],
                        "wind": {"speed": 2.6, "deg": 90},
                        "clouds": {"all": 90}},
                    "list": [
                        {"date": date1,
                            "main": {"temp": 290.45, "humidity": 84, "temp_max": 290.45, "temp_min": 288.56},
                            "weather": [{"id": 803, "main": "Clouds", "description": "broken clouds", "icon": "04n"}],
                            "clouds": 64},
                        {"date": date2,
                            "main": {"temp": 295.12, "humidity": 78, "temp_max": 296.43, "temp_min": 288.01},
                            "weather": [{"id": 803, "main": "Clouds", "description": "broken clouds", "icon": "04d"}],
                            "clouds": 64},
                        {"date": date3, "main": {"temp": 294.13,
                                "humidity": 68, "temp_max": 294.14, "temp_min": 289.82}, "weather": [{"id": 802,
                                    "main": "Clouds", "description": "scattered clouds", "icon": "03d"}], "clouds": 32},
                        {"date": date4, "main": {"temp": 293.42, "humidity": 63, "temp_max": 294.35,
                                "temp_min": 287.37}, "weather": [{"id": 803, "main": "Clouds", "description": "broken clouds", "icon": "04d"}],
                            "clouds": 76},
                        {"date": date5, "main": {"temp": 294.63, "humidity": 0, "temp_max": 294.63,
                                "temp_min": 283.92}, "weather": [{"id": 800, "main": "Clear", "description": "sky is clear", "icon": "01d"}],
                            "clouds": 40},
                        {"date": date6, "main": {"temp": 295.86, "humidity": 0,
                                "temp_max": 295.86, "temp_min": 285.2}, "weather": [{"id": 800, "main": "Clear",
                                    "description": "sky is clear", "icon": "01d"}], "clouds": 58},
                        {"date": date7, "main": {"temp": 292.11, "humidity": 0, "temp_max": 292.11, "temp_min": 286.55},
                            "weather": [{"id": 800, "main": "Clear", "description": "sky is clear", "icon": "01d"}], "clouds": 54}]
                });
                
                currentID += 10;
            }
            
        }
    }

    // Returns an instance of OpenWeatherMap. This instance will be used as singlton in the applicatio
    return WeatherRepositoryHelper;
});
