(function () {
    'use strict';
    var TEST_REGEXP = /(spec|Spec|test|Test)\.js$/i;

    var globalTestInit = [];
    Object.keys(window.__karma__.files).forEach(function (file) {
        if (TEST_REGEXP.test(file)) {
            // Normalize paths to RequireJS module names.
            globalTestInit.push(file);
        }
    });

    requirejs.config({
        baseUrl: '/base/app',
        paths: {
            ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // PATHS FOR SOURCE FILES DEFINITION
            'appservices': 'core/services',
            'library': 'core/lib',
            'presentation': 'ui/scripts',
            'fixtures': 'fixtures',
            'functions': 'core/lib/functions',
            'testhelpers': '../test/helpers',
            ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // CORE DEFINITIONS FOR INTERFACES
            // --- Storages
            'ILocalStorage': 'core/infrastructure/storages/LocalStorage',
            'ISessionStorage': 'core/infrastructure/storages/SessionStorage',
            'ICookieStorage': 'core/infrastructure/storages/CookieStorage',
            'ILocaleRepository': 'core/infrastructure/repositories/custom/LocaleRepository',
            'IWeatherRepository': 'core/infrastructure/repositories/custom/WeatherRepository',
            'IVersionRepository': 'core/infrastructure/repositories/custom/VersionRepository',
            // --- Promises
            'IPromiseAdapter': 'core/infrastructure/promise/adapters/AngularPromiseAdapter',
            // --- Requests
            'IHttpAdapter': 'core/infrastructure/request/adapters/AngularHttpAdapter',
            'IRequestService': 'core/infrastructure/request/GenericRequestServiceAsync',
            'IServiceUrlResolver': 'core/infrastructure/request/ServiceUrlResolver',
            // --- External libs
            'IWeather': 'core/infrastructure/external/OpenWeatherMap',
            ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // APPLICATION LEVEL DEFINITIONS
            // ---- Angular : Core libraries
            'angular': 'vendors/angular/angular',
            'angular-ui-router': 'vendors/angular-ui-router/release/angular-ui-router',
            'angular-bootstrap': 'vendors/angular-bootstrap/ui-bootstrap-tpls.min',
            'angular-touch': 'vendors/angular-touch/angular-touch.min',
            // ---- Angular : translations
            'angular-gettext': 'vendors/angular-gettext/dist/angular-gettext.min',
            'translate': 'ui/scripts/common/modules/extra/translations',
            // ---- Angular : templates
            'templatesmodule': 'ui/scripts/common/modules/templates',
            'templates': 'ui/scripts/common/modules/extra/templates',
            // ---- Require Js
            'requirejs-domready': 'vendors/requirejs-domready/domReady',
            'requirejs-text': 'vendors/requirejs-text/text',
            'async': 'vendors/requirejs-plugins/src/async',
            // ---- Signals (Message bus)
            'moment': 'vendors/moment/min/moment.min',
            'moment-en': 'vendors/moment/locale/en-gb',
            'angular-moment': 'vendors/angular-moment/angular-moment.min',
            // ---- Global libraties : Jquery
            'jquery': 'vendors/jquery/dist/jquery.min',
            // ---- Angular: tyesting
            'angular-mocks': 'vendors/angular-mocks/angular-mocks',
        },
        // angular does not support AMD out of the box, put it in a shim
        shim: {
            'angular': {
                exports: 'angular'
            },
            'angular-ui-router': {
                deps: ['angular']
            },
            'angular-bootstrap': {
                deps: ['angular']
            },
//            'angular-touch': {
//                deps: ['angular']
//            },
            'moment-en': {
                deps: ['moment']
            },
            'angular-moment': {
                deps: ['angular', 'moment']
            },
            'angular-gettext': {
                deps: ['angular']
            },
            'translate': {
                deps: ['angular', 'angular-gettext']
            },
//            'templatesmodule': {
//                deps: ['angular']
//            },
//            'templates': {
//                deps: ['angular', 'templatesmodule']
//            },
            'angular-mocks': {
                deps: ['angular']
            }

        },
        deps: [
            'jquery',
            'functions'
        ].concat(globalTestInit),
        // start test run, once Require.js is done
        callback: window.__karma__.start
    });
})();