// Karma configuration
// http://karma-runner.github.io/0.10/config/configuration-file.html

module.exports = function(config) {
    config.set({
        // base path, that will be used to resolve files and exclude
        basePath: '',
        // testing framework to use (jasmine/mocha/qunit/...)
//        plugins : [
//            'karma-phantomjs-launcher',
//            'karma-mocha',
//            'karma-mocha',
//            'karma-requirejs',
//        ],
        frameworks: ['jasmine', 'requirejs'],
        // junit reporter
        reporters: ['progress', 'junit', 'html'],
        junitReporter: {
            // will be resolved to basePath (in the same way as files/exclude patterns)
            outputFile: 'build/tests/unit/test-results.xml'
        },
        // the default configuration
        htmlReporter: {
            outputDir: 'build/tests/unit/html',
            templatePath: 'node_modules/karma-html-reporter/jasmine_template.html'
        },
        // list of files / patterns to load in the browser
        files: [

//            'app/core/lib/functions.js',
            'node_modules/requirejs/require.js',
            'node_modules/karma-requirejs/lib/adapter.js',
            'app/vendors/requirejs/require.js',
            {pattern: 'app/vendors/angular/angular.js',included: false},
            {pattern: 'app/vendors/angular-mocks/angular-mocks.js',included: false},
            {pattern: 'app/vendors/angular/angular.js',included: false},
            {pattern: 'app/vendors/requirejs-plugins/src/async.js',included: false},
            {pattern: 'app/vendors/jquery/dist/jquery.min.js',included: false},
            {pattern: 'app/vendors/requirejs-domready/domReady.js',included: false},
            {pattern: 'app/vendors/requirejs-text/text.js',included: false},
            {pattern: 'app/vendors/angular-ui-router/release/angular-ui-router.js',included: false},
            {pattern: 'app/vendors/angular-bootstrap/ui-bootstrap-tpls.min.js',included: false},
            {pattern: 'app/vendors/angular-gettext/dist/angular-gettext.min.js',included: false},
            {pattern: 'app/vendors/angular-touch/angular-touch.min.js',included: false},
            {pattern: 'app/vendors/moment/min/moment.min.js',included: false},
            {pattern: 'app/vendors/moment/locale/en-gb.js',included: false},
            {pattern: 'app/vendors/angular-moment/angular-moment.min.js',included: false},
            
            {pattern: 'app/config/*.js', included: false},
            {pattern: 'app/core/infrastructure/**/*.js', included: false},
            {pattern: 'app/core/infrastructure/promise/**/*.js', included: false},
            {pattern: 'app/core/infrastructure/repositories/**/*.js', included: false},
            {pattern: 'app/core/infrastructure/request/**/*.js', included: false},
            {pattern: 'app/core/lib/*.js', included: false},
            {pattern: 'app/core/lib/**/*.js', included: false},
            {pattern: 'app/core/services/*.js', included: false},
            
            {pattern: 'app/ui/scripts/bootstrap.js', included: false},
            {pattern: 'app/ui/scripts/common/directives/*.js', included: false},
            {pattern: 'app/ui/scripts/common/filters/*.js', included: false},
            {pattern: 'app/ui/scripts/common/modules/*.js', included: false},
            {pattern: 'app/ui/scripts/common/modules/extra/*.js', included: false},
            {pattern: 'app/ui/scripts/common/services/*.js', included: false},
            {pattern: 'app/ui/scripts/main/modules/*.js', included: false},
            {pattern: 'app/ui/scripts/main/modules/extra/*.js', included: false},
            {pattern: 'app/ui/scripts/main/controllers/**/*.js', included: false},
            
            
            {pattern: 'app/ui/scripts/main/modules/appcommon.js', included: false},
            {pattern: 'test/unit/main/controllers/**/*.js', included: false},
            {pattern: 'test/mock/**.js', included: false},
            'test/main.js'
        ],
        // list of files / patterns to exclude
        exclude: [
            'app/ui/scripts/main/app.js'
        ],
        // web server port
        port: 8080,
        // level of logging
        // possible values: LOG_DISABLE || LOG_ERROR || LOG_WARN || LOG_INFO || LOG_DEBUG
        logLevel: config.LOG_INFO,
        // enable / disable watching file and executing tests whenever any file changes
        autoWatch: true,
        // Start these browsers, currently available:
        // - Chrome
        // - ChromeCanary
        // - Firefox
        // - Opera
        // - Safari (only Mac)
        // - PhantomJS
        // - IE (only Windows)
        browsers: ['PhantomJS'],

        // If browser does not capture in given timeout [ms], kill it
        captureTimeout: 6000,

        // Continuous Integration mode
        // if true, it capture browsers, run tests and exit
        singleRun: false,
        // enable / disable colors in the output (reporters and logs)
        colors: true,
        // not work with Grunt!!! we need to define all the settings in grunt then
        client: {
            mocha: {
                timeout: 6000, 
                ui: 'bdd'
            }
        }

    });
};
