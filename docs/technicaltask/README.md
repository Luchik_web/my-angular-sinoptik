## Requirements

Design a simple weather application using AngularJS with the following requirements:

- Has a list of pre-set locations (feel free to add a static list of locations of your choice)
- On app load, it should show you the 5-day weather forecast of the default location (refer to designs for desktop and mobile views). Use the [OpenWeatherMap 5 day weather forecast API](http://openweathermap.org/forecast5) to retrieve the weather related data for the specified location
- The user should be able to select a location from the list. On which, that selected locations' 5-day weather info should then be updated on the view
- Add a auto-complete search input where the user can search for a location (as a minimum, limit the location search to the static list that you would have created). Please add states to show if a user enters a location that is not present on the list. 

## What we would like to see from your code:

- How you manage to re-use components without repeating (hint: use angular directives)
- Testable code. Use testing framework of your choice to demonstrate all components pass unit tests. 
- Responsive design. As mentioned, the design should be responsive to adapt to different screen sizes (for the purposes of this exercise, desktop and mobile would be a good minimum)
- How you group related work in terms of git commits

Notes:

- Please put together a good README file which documents how to run, build and test the application on a local dev environment
- Submit your code to a public git repository (github, bitbucket)

## What we are looking for

This exercise is to examine your technical knowledge, reasoning and engineering principals
There are no tricks or hidden agendas
We are looking for a demonstration of your experience and skill using current software development technologies and methodologies
Please make sure your code is clear, tested, documented and demonstrates good practices and industry standards
Also, detail anything further that you would like to achieve with more time
Your solution will form the basis for discussion in subsequent interviews

## Checklist

Please **ensure** you have submitted the following:

1. Working forecast page **as per requirements above**
2. A readme.md explaining
  - How to run / build / test project
  - Explanation of what could be done with more time
3. Project builds / runs / tests as per instruction
4. Submissions not using **AngularJS** will be automatically rejected.

Good luck and thank you for your time - we look forward to seeing your creation.